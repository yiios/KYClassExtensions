//
//  UIView+YPTextList.h
//  YPTextList
//
//  Created by  fireFrog on 16/10/25.
//  Copyright © 2016年  fireFrog. All rights reserved.
//

#import <UIKit/UIKit.h>

static void *YPTextListButtonBlockKey = &YPTextListButtonBlockKey;

typedef void(^YPTextListButtonBlock)(NSString *selectedString);

@interface UIView (YPView)

@property(nonatomic,copy)YPTextListButtonBlock block;

-(UIScrollView *)addTextWithStrings:(NSArray <NSString *>*)stings offset:(CGPoint)offset selectBlock:(YPTextListButtonBlock)block;

@end
