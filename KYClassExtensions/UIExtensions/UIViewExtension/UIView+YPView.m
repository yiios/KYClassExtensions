//
//  UIView+YPTextList.m
//  YPTextList
//
//  Created by  fireFrog on 16/10/25.
//  Copyright © 2016年  fireFrog. All rights reserved.
//

#define kRowHight 33

#import "UIView+YPView.h"
#import <objc/runtime.h>

@implementation UIView (YPView)

-(UIScrollView *)addTextWithStrings:(NSArray <NSString *>*)stings offset:(CGPoint)offset selectBlock:(YPTextListButtonBlock)block{
    self.block = block;
    UIScrollView *contentView = [[UIScrollView alloc]initWithFrame:CGRectMake(CGRectGetMinX(self.frame) + offset.x, CGRectGetMaxY(self.frame) + offset.y, CGRectGetWidth(self.frame), 0)];
    contentView.backgroundColor = [UIColor whiteColor];
    contentView.contentSize = CGSizeMake(CGRectGetWidth(self.frame), kRowHight * stings.count);
    
    [self.superview addSubview:contentView];
    for (int i = 0; i < stings.count; i ++) {
        UIButton *button = [[UIButton alloc]initWithFrame:CGRectMake(0, i * kRowHight, CGRectGetWidth(contentView.frame), kRowHight)];
        [contentView addSubview:button];
        [button addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
        [button setTitle:stings[i] forState:UIControlStateNormal];
        [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        button.titleEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
        
        UIView *lineView = [[UIView alloc]initWithFrame:CGRectMake(CGRectGetMinX(button.frame) + 10, CGRectGetMaxY(button.frame), CGRectGetWidth(button.frame) - 20, 1)];
        [contentView addSubview:lineView];
        lineView.backgroundColor = [UIColor colorWithRed:200/255.f green:200/255.f blue:200/255.f alpha:1];
        
    }
    contentView.layer.borderColor = [UIColor colorWithRed:200/255.f green:200/255.f blue:200/255.f alpha:1].CGColor;
    contentView.layer.borderWidth = 1;
    
    CGRect rect = CGRectMake(CGRectGetMinX(self.frame) +offset.x, CGRectGetMaxY(self.frame) + offset.y, CGRectGetWidth(self.frame), kRowHight * stings.count);
    
    if (CGRectGetHeight(rect) > kRowHight * 5) {
        rect = CGRectMake(CGRectGetMinX(self.frame) + offset.x, CGRectGetMaxY(self.frame) + offset.y, CGRectGetWidth(self.frame), kRowHight * 5);
    }
    
    [UIView animateWithDuration:0.5 animations:^{
        contentView.frame = rect;
    }];
    return contentView;
}

-(void)buttonAction:(UIButton *)sender{
    if (self.block) {
        self.block(sender.currentTitle);
    }
    
    [UIView animateWithDuration:0.5 animations:^{
        sender.superview.frame = CGRectMake(CGRectGetMinX(sender.superview.frame), CGRectGetMinY(sender.superview.frame), CGRectGetWidth(sender.superview.frame), 0);
    } completion:^(BOOL finished) {
        [sender.superview removeFromSuperview];
    }];
}

-(void)setBlock:(YPTextListButtonBlock)block{
    objc_setAssociatedObject(self, &YPTextListButtonBlockKey, block, OBJC_ASSOCIATION_COPY);
}
-(YPTextListButtonBlock)block{
    return objc_getAssociatedObject(self, &YPTextListButtonBlockKey);
}


@end
