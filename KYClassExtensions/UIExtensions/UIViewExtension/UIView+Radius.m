//
//  UIView+Radius.m
//  ClassExtensions
//
//  Created by zhigang on 17/4/17.
//  Copyright © 2017年 King. All rights reserved.
//

#import "UIView+Radius.h"

@implementation UIView (Radius)

- (void)doCircleFrame{
    self.layer.masksToBounds = YES;
    self.layer.cornerRadius = self.frame.size.width/2;
}

- (void)cornerRadius:(CGFloat)cornerRadius
         borderColor:(CGColorRef)borderColor
         borderWidth:(CGFloat)borderWidth
{
    self.clipsToBounds = YES;
    self.layer.cornerRadius = cornerRadius;
    self.layer.borderColor = borderColor;
    self.layer.borderWidth = borderWidth;
    
}
- (void)cornerRadiusToNumber:(NSInteger)integer{
    self.layer.masksToBounds = YES;
    self.layer.cornerRadius = integer;
    //    self.layer.borderWidth = 0.5;
}


- (void)msCommonBorder{
    self.layer.borderWidth = 1;
    self.layer.borderColor = [UIColor colorWithRed:(double)207/255.0f green:(double)207/255.0f blue:(double)207/255.0f alpha:1].CGColor;
    self.layer.cornerRadius = 5;
    self.layer.masksToBounds = YES;
    
}
@end
