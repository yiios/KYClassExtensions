//
//  UIView+Common.m
//  kyExpress
//
//  Created by 陈志刚 on 15/12/5.
//  Copyright © 2015年 kyExpress. All rights reserved.
//

#import "UIView+Common.h"
#import "JRSwizzle.h"

@implementation UIView (Common)

- (void)doCircleFrame{
    self.layer.masksToBounds = YES;
    self.layer.cornerRadius = self.frame.size.width/2;
//    self.layer.borderWidth = 0.5;
}

- (void)cornerRadiusToNumber:(NSInteger)integer{
    self.layer.masksToBounds = YES;
    self.layer.cornerRadius = integer;
//    self.layer.borderWidth = 0.5;
}


- (void)msCommonBorder{
    self.layer.borderWidth = 1;
    self.layer.borderColor = [UIColor colorWithRed:(double)207/255.0f green:(double)207/255.0f blue:(double)207/255.0f alpha:1].CGColor;
    self.layer.cornerRadius = 5;
    self.layer.masksToBounds = YES;

}




#ifdef __IPHONE_11_0
+ (void)load {
     [NSClassFromString(@"_UIBackButtonContainerView")     jr_swizzleMethod:@selector(addSubview:) withMethod:@selector(iOS11BackButtonNoTextTrick_addSubview:) error:nil];
}
- (void)iOS11BackButtonNoTextTrick_addSubview:(UIView *)view {
    view.alpha = 0;
    if ([view isKindOfClass:[UIButton class]]) {
        UIButton *button = (id)view;
        [button setTitle:@" " forState:UIControlStateNormal];
    }
    [self iOS11BackButtonNoTextTrick_addSubview:view];
    
}
#endif


@end
