//
//  UIView+Common.h
//  kyExpress
//
//  Created by 陈志刚 on 15/12/5.
//  Copyright © 2015年 kyExpress. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Common)

/**
 *  设置原型
 */
- (void)doCircleFrame;
/**
 *  设置圆角弧度
 *
 *  @param integer 弧度数
 */
- (void)cornerRadiusToNumber:(NSInteger)integer;

//边距宽度为1，颜色为灰色，圆角为3的边框
- (void)msCommonBorder;

@end
