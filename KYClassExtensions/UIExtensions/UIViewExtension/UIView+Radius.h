//
//  UIView+Radius.h
//  ClassExtensions
//
//  Created by zhigang on 17/4/17.
//  Copyright © 2017年 King. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Radius)

/*** 设置视图为正圆形 */
- (void)doCircleFrame;

/**
 *  设置圆角弧度边线颜色线宽
 *  @param cornerRadius 弧度数
 *  @param borderColor 边框颜色
 *  @param borderWidth 边框宽度
 */
- (void)cornerRadius:(CGFloat)cornerRadius
         borderColor:(CGColorRef)borderColor
         borderWidth:(CGFloat)borderWidth;
/**
 *  设置圆角弧度
 *
 *  @param integer 弧度数
 */
- (void)cornerRadiusToNumber:(NSInteger)integer;

//边距宽度为1，颜色为灰色，圆角为3的边框
- (void)msCommonBorder;
@end
