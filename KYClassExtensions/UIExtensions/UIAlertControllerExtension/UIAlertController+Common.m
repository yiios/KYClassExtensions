//
//  UIAlertController+Common.m
//  kyExpress_Internal
//
//  Created by MelissaShu on 17/3/31.
//  Copyright © 2017年 kyExpress. All rights reserved.
//

#import "UIAlertController+Common.h"
#import "UIColor+HexString.h"

@implementation UIAlertController (Common)




+ (UIAlertController*)bk_showAlertControllerWithTitle:(NSString *)title
                                          message:(NSString *)message
                                cancelButtonTitle:(NSString *)cancelButtonTitle
                                    otherButtonTitles:(NSArray *)otherButtonTitles
                                       sureAction:(void (^)())sAction
                                     cancelAction:(void (^)())cAction
{
    
//    NSString *title=NSLocalizedString(@"Objective-C", nil);
//    NSString *tipContent=NSLocalizedString(@"FlyElephant", nil);
    UIAlertController *alertController=[UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];

    
    UIAlertAction *sureAction=[UIAlertAction actionWithTitle:NSLocalizedString(otherButtonTitles[0], nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
       
        sAction();
    }];
    
    UIAlertAction *cancelAction=[UIAlertAction actionWithTitle:NSLocalizedString(cancelButtonTitle, nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
       
        cAction();
        
    }];
    [cancelAction setValue:[UIColor colorWithHexString:@"0x7c0fbb"] forKey:@"titleTextColor"];
    [sureAction setValue:[UIColor colorWithHexString:@"0x7c0fbb"] forKey:@"titleTextColor"];
    
    [alertController addAction:sureAction];
    [alertController addAction:cancelAction];
   
//    UIViewController *currentVc = [self getCurrentVC];
//    currentVc.editing = YES;
//    NSLog(@"currentVc : %@",currentVc);
//    
//    [currentVc presentViewController:vc animated:YES completion:^{
//        //        NSLog(@"已跳转");
//    }];
    
    return alertController;
}


////获取当前屏幕显示的viewcontroller
//- (UIViewController *)getCurrentVC
//{
//    //获得当前活动窗口的根视图
//    UIViewController* vc = [UIApplication sharedApplication].keyWindow.rootViewController;
//    while (1)
//    {
//        //根据不同的页面切换方式，逐步取得最上层的viewController
//        if ([vc isKindOfClass:[UITabBarController class]]) {
//            vc = ((UITabBarController*)vc).selectedViewController;
//        }
//        if ([vc isKindOfClass:[UINavigationController class]]) {
//            vc = ((UINavigationController*)vc).visibleViewController;
//        }
//        if (vc.presentedViewController) {
//            vc = vc.presentedViewController;
//        }else{
//            break;
//        }
//    }
//    return vc;
//}


@end
