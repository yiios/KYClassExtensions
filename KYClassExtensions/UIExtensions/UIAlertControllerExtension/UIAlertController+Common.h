//
//  UIAlertController+Common.h
//  kyExpress_Internal
//
//  Created by MelissaShu on 17/3/31.
//  Copyright © 2017年 kyExpress. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIAlertController (Common)

+ (UIAlertController*)bk_showAlertControllerWithTitle:(NSString *)title
                                              message:(NSString *)message
                                    cancelButtonTitle:(NSString *)cancelButtonTitle
                                    otherButtonTitles:(NSArray *)otherButtonTitles
                                           sureAction:(void (^)())sAction
                                         cancelAction:(void (^)())cAction;

@end
