//
//  UISearchBar+ViewSetting.m
//  ClassExtensions
//
//  Created by zhigang on 17/4/17.
//  Copyright © 2017年 King. All rights reserved.
//

#import "UISearchBar+ViewSetting.h"

@implementation UISearchBar (ViewSetting)

/**
 *	@brief	 设置searchBar 背景透明
 */
- (void)searchBarToClearWithSearchBar{
    
    for (UIView *view in self.subviews) {
        if([[[UIDevice currentDevice] systemVersion] compare:@"7.0"] != NSOrderedAscending){
            if ([view isKindOfClass:NSClassFromString(@"UIView")] && view.subviews.count > 0) {
                [[view.subviews objectAtIndex:0] removeFromSuperview];
                break;
            }
        }else{
            if ([view isKindOfClass:NSClassFromString(@"UISearchBarBackground")]) {
                [view removeFromSuperview];
                break;
            }
        }
    }
}


@end
