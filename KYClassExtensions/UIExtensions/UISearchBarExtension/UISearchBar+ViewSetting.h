//
//  UISearchBar+ViewSetting.h
//  ClassExtensions
//
//  Created by zhigang on 17/4/17.
//  Copyright © 2017年 King. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UISearchBar (ViewSetting)

/**
 *	@brief	 设置searchBar 背景透明
 */
- (void)searchBarToClearWithSearchBar;

@end
