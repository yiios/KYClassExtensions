//
//  UIColor+AttributeHelper.m
//  ClassExtensions
//
//  Created by zhigang on 17/4/18.
//  Copyright © 2017年 King. All rights reserved.
//

#import "UIColor+AttributeHelper.h"

@implementation UIColor (AttributeHelper)

+ (instancetype)randomColor
{
    return [UIColor colorWithRed:arc4random_uniform(256) / 255.f
                           green:arc4random_uniform(256) / 255.f
                            blue:arc4random_uniform(256) / 255.f
                           alpha:1.f];
}

@end
