//
//  UIColor+Common.h
//  King
//
//  Created by King on 15/6/9.
//  Copyright (c) 2015年 c521xiong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Common)

/**
 *  根据16进制返回颜色
 *
 *  @param hex 16进制
 *
 *  @return UIColor
 */
+ (UIColor *)colorWithRGBHex:(UInt32)hex;

/**
 *  根据16进制字符串返回颜色
 *
 *  @param stringToConvert 16进制字符串
 *
 *  @return UIColor
 */
+ (UIColor *)colorWithHexString:(NSString *)stringToConvert;

/**
 *  根据16进制字符串返回待透明度的颜色
 *
 *  @param stringToConvert 16进制字符串
 *  @param alpha           透明度
 *
 *  @return UIColor
 */
+ (UIColor *)colorWithHexString:(NSString *)stringToConvert andAlpha:(CGFloat)alpha;

@end
