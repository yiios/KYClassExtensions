//
//  UIColor+Extensior.m
//  我的评分
//
//  Created by 夏以铭 on 16/8/31.
//  Copyright © 2016年 alangavin. All rights reserved.
//

#import "UIColor+Extensior.h"

@implementation UIColor (Extensior)

+(instancetype)Ky_colorWithHex:(u_int32_t)hex{
    int red=(hex & 0xFF0000) >> 16;
    int green=(hex & 0x00FF00) >> 8;
    int blue=hex & 0x0000FF;
    
    return  [UIColor colorWithRed:(red/ 255.0) green:(green / 255.0) blue:(blue / 255.0) alpha:1.0] ;
}

+ (UIColor *) colorWithHexString: (NSString *)hexColor
{
    NSString *cString = [[hexColor stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    // String should be 6 or 8 characters
    if ([cString length] < 6) {
        return [UIColor clearColor];
    }
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"])
        cString = [cString substringFromIndex:2];
    if ([cString hasPrefix:@"#"])
        cString = [cString substringFromIndex:1];
    if ([cString length] != 6)
        return [UIColor clearColor];
    // Separate into r, g, b substrings
    unsigned int red,green,blue;
    NSRange range;
    range.location = 0;
    range.length = 2;
    //r
    [[NSScanner scannerWithString:[cString substringWithRange:range]] scanHexInt:&red];
    //g
    range.location = 2;
    [[NSScanner scannerWithString:[cString substringWithRange:range]] scanHexInt:&green];
    //b
    range.location = 4;
    [[NSScanner scannerWithString:[cString substringWithRange:range]] scanHexInt:&blue];
    
    return [UIColor colorWithRed:(float)(red/255.0f) green:(float)(green / 255.0f) blue:(float)(blue / 255.0f) alpha:1.0f];
}

@end
