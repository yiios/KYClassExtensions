//
//  UIColor+AttributeHelper.h
//  ClassExtensions
//
//  Created by zhigang on 17/4/18.
//  Copyright © 2017年 King. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (AttributeHelper)

// 随机颜色
+ (instancetype)randomColor;

@end
