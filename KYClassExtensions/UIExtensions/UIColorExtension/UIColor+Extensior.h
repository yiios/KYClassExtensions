//
//  UIColor+Extensior.h
//  我的评分
//
//  Created by 夏以铭 on 16/8/31.
//  Copyright © 2016年 alangavin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Extensior)

+(instancetype)Ky_colorWithHex:(u_int32_t)hex;

/**
 *  十六进制转Color
 *
 *  @param hexColor 十六进制
 */
+ (UIColor *) colorWithHexString: (NSString *)hexColor;
@end
