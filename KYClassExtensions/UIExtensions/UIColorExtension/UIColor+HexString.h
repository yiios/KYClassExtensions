//
//  UIColor+HexString.h
//  ClassExtensions
//
//  Created by zhigang on 17/4/17.
//  Copyright © 2017年 King. All rights reserved.
//

#import <UIKit/UIKit.h>

/*
    根据数字来生成颜色
 */

@interface UIColor (HexString)

/**
 *  根据16进制返回颜色
 *
 *  @param hex 16进制
 *
 *  @return UIColor
 */
+ (instancetype)Ky_colorWithHex:(u_int32_t)hex;
+ (UIColor *)colorWithRGBHex:(UInt32)hex;

/**
 *  根据16进制字符串返回颜色
 *
 *  @param stringToConvert 16进制字符串
 *
 *  @return UIColor
 */
+ (UIColor *)colorWithHexString:(NSString *)stringToConvert;

/**
 *  根据16进制字符串返回待透明度的颜色
 *
 *  @param stringToConvert 16进制字符串
 *  @param alpha           透明度
 *
 *  @return UIColor
 */
+ (UIColor *)colorWithHexString:(NSString *)stringToConvert andAlpha:(CGFloat)alpha;

@end
