//
//  UITextView+PlaceHolder.h
//  kyExpress_Internal
//
//  Created by 林晓斌 on 2016/12/5.
//  Copyright © 2016年 kyExpress. All rights reserved.
//

#import <UIKit/UIKit.h>

FOUNDATION_EXPORT double UITextView_PlaceholderVersionNumber;
FOUNDATION_EXPORT const unsigned char UITextView_PlaceholderVersionString[];

@interface UITextView (PlaceHolder)
@property (nonatomic, readonly) UILabel *placeholderLabel;
@property (nonatomic, strong) IBInspectable NSString *placeholder;
@property (nonatomic, strong) NSAttributedString *attributedPlaceholder;
@property (nonatomic, strong) IBInspectable UIColor *placeholderColor;
@property(nonatomic,copy)NSString *isDD;

+ (UIColor *)defaultPlaceholderColor;

@end
