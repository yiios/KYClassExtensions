//
//  UITextView+myTextView.m
//  kyExpress
//
//  Created by xiegang on 16/5/6.
//  Copyright © 2016年 kyExpress. All rights reserved.
//

#import "UITextView+myTextView.h"
#import<objc/runtime.h>

@interface UITextView()

@property (nonatomic,strong)NSString * isNeedTopMume;//是否需要长按菜单

@end

static const char *isneed = "isneed";

@implementation UITextView (myTextView)


- (NSString * )isNeedTopMume{
    return (NSString *)objc_getAssociatedObject(self, isneed);
}


- (void)setIsNeedTopMume:(NSString *)isNeedTopMume{
    
    objc_setAssociatedObject(self, isneed, isNeedTopMume, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    
}

- (void)haveNeedMune:(BOOL)isneedTopmeue
{
    if(isneedTopmeue){
        self.isNeedTopMume = @"yes";
    }else{
        self.isNeedTopMume = @"no";
    }
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender {
    
    if([self.isNeedTopMume isEqualToString:@"no"]){
        
        return NO;
        
    }
    
    //开启5个正常模式：拷贝，剪切，全选，选择，粘贴
    if((action == @selector(copy:)||action == @selector(cut:)||action == @selector(selectAll:)||action == @selector(select:)||action == @selector(paste:))&&!self.secureTextEntry){
        
        return YES;
        
    }
    //密文模式不能拷贝与剪切
    if(self.secureTextEntry ==YES){
        if(action == @selector(copy:)||action == @selector(cut:)){
            
            return NO;
        }
        if(action == @selector(selectAll:)||action == @selector(select:)||action == @selector(paste:)){
            
            return YES;
        }
    }
    //其余全部关闭
    return NO;
}

@end
