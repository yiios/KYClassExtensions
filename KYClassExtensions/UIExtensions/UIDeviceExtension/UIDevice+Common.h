//
//  UIDevice+Common.h
//  King
//
//  Created by King on 15/6/9.
//  Copyright (c) 2015年 c521xiong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIDevice (Common)

+ (NSDictionary *)systemInfoDict;

@end
