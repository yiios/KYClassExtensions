//
//  UIDevice+PermissionsHelper.h
//  ClassExtensions
//
//  Created by zhigang on 17/4/17.
//  Copyright © 2017年 King. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIDevice (PermissionsHelper)

/**
 *  是否允许访问通讯录
 */
+ (BOOL)isAllowedToAccessAddressBook;
/**
 *  设备信息
 */
+ (NSDictionary *)systemInfoDict;

@end
