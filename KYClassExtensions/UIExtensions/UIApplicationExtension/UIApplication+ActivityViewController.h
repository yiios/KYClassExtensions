//
//  UIApplication+ActivityViewController.h
//  kyExpress_Internal
//
//  Created by iOS_Chris on 17/3/10.
//  Copyright © 2017年 kyExpress. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIApplication (ActivityViewController)

- (UIViewController *)p_nextTopForViewController:(UIViewController *)inViewController;
- (UIViewController *)activityViewController;

@end
