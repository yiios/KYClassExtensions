//
//  UIImage+ColorImage.h
//  ClassExtensions
//
//  Created by zhigang on 17/4/17.
//  Copyright © 2017年 King. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (ColorImage)

/**
 *  根据颜色来生成一张纯色的图片
 *
 *  @param aColor 颜色
 *
 *  @return 图片
 */
+ (UIImage *)imageWithColor:(UIColor *)aColor;

@end
