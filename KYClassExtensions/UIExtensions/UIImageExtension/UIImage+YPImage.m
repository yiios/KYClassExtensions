//
//  UIImage+YPImage.m
//  kyExpress_Internal
//
//  Created by yeyongping on 2016/11/12.
//  Copyright © 2016年 kyExpress. All rights reserved.
//

#import "UIImage+YPImage.h"

@implementation UIImage (YPImage)

/**
 *  图片压缩分辨率
 */
- (UIImage *)compressToMaxDataSize:(NSInteger)maxSize{
    //注意:这里的单位为KB
    //先调整分辨率
    CGSize newSize = CGSizeMake(self.size.width, self.size.height);
    
    CGFloat tempHeight = newSize.height / 200;
    CGFloat tempWidth = newSize.width / 200;
    
    if (tempWidth > 1.0 && tempWidth > tempHeight) {
        newSize = CGSizeMake(self.size.width / tempWidth, self.size.height / tempWidth);
    }else if (tempHeight > 1.0 && tempWidth < tempHeight){
        newSize = CGSizeMake(self.size.width / tempHeight, self.size.height / tempHeight);
    }
    
    UIGraphicsBeginImageContext(newSize);
    [self drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    //调整大小
    NSData *imageData = UIImageJPEGRepresentation(newImage,1.0);
    NSUInteger sizeOrigin = [imageData length];
    NSUInteger sizeOriginKB = sizeOrigin / 1024;
    
    float i = 0.5;
    // 循环计数器 避免压缩不到所要值 一致压缩的情况出现
    NSInteger whileIndex = 0;
    while (sizeOriginKB > maxSize) {
        
        whileIndex++;
        
        imageData = UIImageJPEGRepresentation(newImage, i);
        sizeOriginKB = [imageData length] / 1024;
        i *= 0.5;
        
        if (whileIndex > 20) {
            break;
        }
    }
    return [UIImage imageWithData:imageData];
}


/**
 *  图片压缩分辨率(不改变尺寸)
 */
- (UIImage *)compressToDataSize:(NSInteger)maxSize{
    //注意:这里的单位为KB
    //先调整分辨率
    CGSize newSize = CGSizeMake(self.size.width, self.size.height);
    
    CGFloat tempHeight = newSize.height/1024;
    CGFloat tempWidth = newSize.width/1024;
    
    if (tempWidth > 1.0 && tempWidth > tempHeight) {
        newSize = CGSizeMake(self.size.width / tempWidth, self.size.height / tempWidth);
    }else if (tempHeight > 1.0 && tempWidth < tempHeight){
        newSize = CGSizeMake(self.size.width / tempHeight, self.size.height / tempHeight);
    }
    
    UIGraphicsBeginImageContext(newSize);
    [self drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    //调整大小
    NSData *imageData = UIImageJPEGRepresentation(newImage,1.0);
    NSUInteger sizeOrigin = [imageData length];
    NSUInteger sizeOriginKB = sizeOrigin / 1024;
    
    float i = 0.5;
    while (sizeOriginKB > maxSize) {
        imageData = UIImageJPEGRepresentation(newImage, i);
        sizeOriginKB = [imageData length] / 1024;
        i *= 0.5;
    }
    return [UIImage imageWithData:imageData];
}



/**
 *  图片压缩到指定尺寸
 */
- (UIImage*)compressToCGSize:(CGSize)targetSize
{
    UIImage *sourceImage = self;
    UIImage *newImage = nil;
    CGSize imageSize = sourceImage.size;
    CGFloat width = imageSize.width;
    CGFloat height = imageSize.height;
    CGFloat targetWidth = targetSize.width;
    CGFloat targetHeight = targetSize.height;
    CGFloat scaleFactor = 0.0;
    CGFloat scaledWidth = targetWidth;
    CGFloat scaledHeight = targetHeight;
    CGPoint thumbnailPoint = CGPointMake(0.0,0.0);
    
    if (CGSizeEqualToSize(imageSize, targetSize) == NO){
        CGFloat widthFactor = targetWidth / width;
        CGFloat heightFactor = targetHeight / height;
        
        if (widthFactor > heightFactor){
            scaleFactor = widthFactor;
        }else{
            scaleFactor = heightFactor;
        }
        scaledWidth= width * scaleFactor;
        scaledHeight = height * scaleFactor;
        
        if (widthFactor > heightFactor){
            thumbnailPoint.y = (targetHeight - scaledHeight) * 0.5;
        } else if (widthFactor < heightFactor){
            thumbnailPoint.x = (targetWidth - scaledWidth) * 0.5;
        }
    }
    
    UIGraphicsBeginImageContext(targetSize); // this will crop
    
    CGRect thumbnailRect = CGRectZero;
    thumbnailRect.origin = thumbnailPoint;
    thumbnailRect.size.width= scaledWidth;
    thumbnailRect.size.height = scaledHeight;
    
    [sourceImage drawInRect:thumbnailRect];
    
    newImage = UIGraphicsGetImageFromCurrentImageContext();
    if(newImage == nil)
        NSLog(@"不能压缩");
    UIGraphicsEndImageContext();
    return newImage;
}

- (NSString *)JPEGBase64String{
    NSData *data = [UIImageJPEGRepresentation(self, 1) base64EncodedDataWithOptions:NSDataBase64EncodingEndLineWithLineFeed];
    return [NSString stringWithUTF8String:[data bytes]];
}

-(NSString *)PNGBase64String{
    NSData * data = [UIImagePNGRepresentation(self) base64EncodedDataWithOptions:NSDataBase64EncodingEndLineWithLineFeed];
    return [NSString stringWithUTF8String:[data bytes]];
}

@end
