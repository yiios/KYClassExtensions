//
//  UIImage+Wechat.h
//
//  Created by tiger on 2017/2/21.
//  Copyright © 2017年 xinma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (WechatCompress)

/**
 use session compress Strategy
 */
- (UIImage *)wcSessionCompress;

/**
 use timeline compress Strategy
 */
- (UIImage *)wcTimelineCompress;


/**
 wechat image compress
 
 @param isSession session image boundary is 800, timeline is 1280
 
 @return thumb imageData
 */
- (NSData *)wcCompresToData:(Boolean)isSession;


/**
 get wechat compress image size
 
 @return thumb image size
 */
- (CGSize)wxImageSize:(Boolean)isSession ;

/**
 Zoom the picture to the specified size
 
 @param newSize session image boundary is 800, timeline is 1280
 
 @return new image
 */
- (UIImage *)resizedImage:(CGSize)newSize;
@end
