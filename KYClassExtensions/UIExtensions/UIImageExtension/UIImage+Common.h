//
//  UIImage+Common.h
//  kyExpress_Internal
//
//  Created by iOS_Chris on 16/3/1.
//  Copyright © 2016年 kyExpress. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Common)

/**
 *  根据颜色来生成一张纯色的图片
 *
 *  @param aColor 颜色
 *
 *  @return 图片
 */
+ (UIImage *)imageWithColor:(UIColor *)aColor;
+ (UIImage *)fixOrientation:(UIImage *)aImage;
@end
