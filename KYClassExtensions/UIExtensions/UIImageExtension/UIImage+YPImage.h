//
//  UIImage+YPImage.h
//  kyExpress_Internal
//
//  Created by yeyongping on 2016/11/12.
//  Copyright © 2016年 kyExpress. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (YPImage)

- (UIImage *)compressToMaxDataSize:(NSInteger)maxSize;
- (UIImage*)compressToCGSize:(CGSize)targetSize;
- (UIImage *)compressToDataSize:(NSInteger)maxSize;
- (NSString *)JPEGBase64String;
- (NSString *)PNGBase64String;
@end
