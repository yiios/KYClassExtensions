/*
     File: UIImage+ImageEffects.h
 Abstract: This is a category of UIImage that adds methods to apply blur and tint effects to an image. This is the code you’ll want to look out to find out how to use vImage to efficiently calculate a blur.
  Version: 1.0
*/

#import <UIKit/UIKit.h>

@interface UIImage (ImageEffects)

/*** 毛玻璃效果 */
- (UIImage *)applyLightEffect;
/*** 模糊毛玻璃效果 */
- (UIImage *)applyExtraLightEffect;
/*** 毛玻璃效果 */
- (UIImage *)applyDarkEffect;

- (UIImage *)applyTintEffectWithColor:(UIColor *)tintColor;

- (UIImage *)applyBlurWithRadius:(CGFloat)blurRadius tintColor:(UIColor *)tintColor saturationDeltaFactor:(CGFloat)saturationDeltaFactor maskImage:(UIImage *)maskImage;

@end

@interface UIImage (SnapshotImage)

+ (UIImage *)snapshotImageWithView:(UIView *)view;

@end

