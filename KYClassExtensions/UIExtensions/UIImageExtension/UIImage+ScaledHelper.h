//
//  UIImage+ScaledHelper.h
//  ClassExtensions
//
//  Created by zhigang on 17/4/17.
//  Copyright © 2017年 King. All rights reserved.
//

#import <UIKit/UIKit.h>

/*
    图片压缩/尺寸/ 处理
 */

@interface UIImage (ScaledHelper)

/**
 *  对图片尺寸进行压缩
 *
 *  @param targetSize 压缩后的尺寸
 *
 *  @return UIImage
 */
- (UIImage*)scaledToSize:(CGSize)targetSize;

/**
 *  是否对对图片尺寸进行高质量的压缩
 *
 *  @param targetSize  压缩后的尺寸
 *  @param highQuality 压缩后的尺寸 * 2
 *
 *  @return UIImage
 */
- (UIImage*)scaledToSize:(CGSize)targetSize highQuality:(BOOL)highQuality;

/**
 *  根据最大的尺寸是否缩放
 *
 *  @param size CGSize
 *
 *  @return UIImage
 */
- (UIImage*)scaledToMaxSize:(CGSize )size;

/**
 *  转化为8位图片(黑白图片)
 *
 *  @return UIImage
 */
- (UIImage*)convertImageToGrey;

/**
 *   截屏 并压缩质量 0 则为不压缩
 *
 *  @param scale .1 ~ 1 之间   0 则为不压缩
 *
 *  @return 压缩后的图片
 */
+ (UIImage *)captureScreenAndScaledToFloat:(CGFloat)scale;




@end
