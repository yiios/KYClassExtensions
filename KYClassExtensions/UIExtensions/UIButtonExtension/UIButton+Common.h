//
//  UIButton+Common.h
//  kyExpress_Internal
//
//  Created by iOS_Chris on 17/3/7.
//  Copyright © 2017年 kyExpress. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (Common)

//底部按钮
- (void)msBottomButton;


//提交按钮-开始动画效果
- (void)commitBtnAnimationStart;
//提交按钮-结束动画效果
- (void)commitBtnAnimationStop;
//提交按钮-结束动画效果;设置按钮标题
- (void)commitBtnAnimationStopWithTitle:(NSString *)title;
@end
