//
//  UIButton+Common.m
//  kyExpress_Internal
//
//  Created by iOS_Chris on 17/3/7.
//  Copyright © 2017年 kyExpress. All rights reserved.
//
#import "UIButton+Common.h"
#import "NSString+CharacterProcessingHelper.h"
#import <objc/runtime.h>
#import "UIColor+HcdCustom.h"


static char const * const kOriginTitle = "originTitle";

@interface UIButton()

@property (nonatomic, copy) NSString *originTitle; //按钮的原始标题

//@property (nonatomic, strong) CABasicAnimation *rotationAnimation;

@end

@implementation UIButton (Common)

//@dynamic originTitle;

- (void)msBottomButton{
    
    self.layer.borderWidth = 1;
    
    self.layer.cornerRadius = 5;
    self.layer.masksToBounds = YES;
    
    self.backgroundColor = [UIColor colorWithHexString:@"0x7c0fbb"];
    self.layer.borderColor = [UIColor colorWithHexString:@"0x7c0fbb"].CGColor;
    
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.titleLabel.font = [UIFont systemFontOfSize:16.0f];

}


//提交按钮-开始动画效果
- (void)commitBtnAnimationStart{
    
    [self setImage:[UIImage imageNamed:@"rotate"] forState:UIControlStateNormal];
    
    NSString *title = self.titleLabel.text;
    self.originTitle = title;
    
    NSLog(@"originTitle0 : %@",self.originTitle);
    
    [self setTitle:[NSString stringWithFormat:@"  正在%@...",title] forState:UIControlStateNormal];
    
    [self setBackgroundColor:[UIColor colorWithHexString:@"510080"]];
    CABasicAnimation *rotationAnimation=  [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.toValue = [NSNumber numberWithFloat: M_PI * 2.0 ];
    rotationAnimation.duration = 1;
    rotationAnimation.cumulative = YES;
    rotationAnimation.repeatCount = HUGE_VALF;
    
    [self.imageView.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
    
    self.enabled = NO;
}

//提交按钮-结束动画效果
- (void)commitBtnAnimationStop{
    [self.imageView.layer removeAllAnimations];
    [self setBackgroundColor:[UIColor colorWithHexString:@"0x7c0fbb"]];
    [self setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    
    
    NSString *originTitle = objc_getAssociatedObject(self, kOriginTitle);
    NSLog(@"originTitle1 : %@",originTitle);
    
    if ([originTitle isEmpty]) {
        
        [self setTitle:@"提交" forState:UIControlStateNormal];
    }else{
        
        [self setTitle:self.originTitle forState:UIControlStateNormal];
    }
    
    self.enabled = YES;
}

-(void)setOriginTitle:(NSString *)originTitle
{
    objc_setAssociatedObject(self, kOriginTitle, originTitle, OBJC_ASSOCIATION_COPY);

}

- (NSString *)originTitle
{
    return objc_getAssociatedObject(self, kOriginTitle);
}

//提交按钮-结束动画效果;设置按钮标题
- (void)commitBtnAnimationStopWithTitle:(NSString *)title{
    
    if ([title isEmpty]) {
        [self commitBtnAnimationStop];
        return;
    }
    
    [self.imageView.layer removeAllAnimations];
    [self setBackgroundColor:[UIColor colorWithHexString:@"0x7c0fbb"]];
    [self setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    [self setTitle:title forState:UIControlStateNormal];
    
    self.enabled = YES;
}


@end
