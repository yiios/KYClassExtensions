/**
 * Copyright 2016 Hanju Network Technologies Co., Ltd
 * http://www.hanju-tech.com/
 *
 * 一句话功能简述
 * 功能详细描述
 *
 * @author  李少鹏 lishaopeng@hanju-tech.com
 * @version 0.1 16/5/27 上午午10:12
 */

#import <UIKit/UIKit.h>

@interface UIButton (BackGroundColor)
- (void)setBackgroundColor:(UIColor *)backgroundColor forState:(UIControlState)state;


@end
