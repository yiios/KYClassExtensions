/**
 * Copyright 2016 Hanju Network Technologies Co., Ltd
 * http://www.hanju-tech.com/
 *
 *改变UIbutton不同状态下的背景色
 *
 * @author  李少鹏 lishaopeng@hanju-tech.com
 * @version 0.1 16/5/27 上午午10:12
 */

#import "UIButton+BackGroundColor.h"

@implementation UIButton (BackGroundColor)
- (void)setBackgroundColor:(UIColor *)backgroundColor forState:(UIControlState)state {
    [self setBackgroundImage:[UIButton imageWithColor:backgroundColor] forState:state];
}
+ (UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
     
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
     
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
     
    return image;
}

@end
