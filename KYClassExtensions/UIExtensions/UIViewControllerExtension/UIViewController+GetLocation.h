//
//  UIViewController+GetLocation.h
//  SZESS1217
//
//  Created by 魏信洋 on 16/1/10.
//  Copyright © 2016年 shanreal. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^GetLocationStr)(NSString *locationStr);

@interface UIViewController (GetLocation)

@property (nonatomic, copy) GetLocationStr getLocationStr;

- (void)getLocationStrWithComplete:(GetLocationStr)complete;


// 添加控制器
- (void)wk_addChildViewController:(UIViewController *)childController;
// 删除视图控制器
- (void)wk_removeFromParentViewController;

@end
