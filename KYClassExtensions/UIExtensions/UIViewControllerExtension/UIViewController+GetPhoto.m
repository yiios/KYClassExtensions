//
//  UIViewController+GetPhoto.m
//
//  Copyright © 2015年 kylin. All rights reserved.
//

#import "UIViewController+GetPhoto.h"
#import <objc/runtime.h>
#import <AVFoundation/AVFoundation.h>

static const char *CompleteBlockKey = "CompleteBlockKey";
static const char *CancelBlockKey = "CancelBlockKey";

@interface UIViewController ()<UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIActionSheetDelegate>

@end

@implementation UIViewController (GetPhoto)

- (CompleteBlock)completeBlock {
    return (CompleteBlock)objc_getAssociatedObject(self, CompleteBlockKey);
}

- (void)setCompleteBlock:(CompleteBlock)completeBlock {
    objc_setAssociatedObject(self, CompleteBlockKey, completeBlock, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (CancelBlock)cancelBlock {
    return (CancelBlock)objc_getAssociatedObject(self, CancelBlockKey);
}

- (void)setCancelBlock:(CancelBlock)cancelBlock {
    objc_setAssociatedObject(self, CancelBlockKey, cancelBlock, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

/// 打开图片选择菜单
- (void)chooseImageWithComplete:(CompleteBlock)completeBlock Cancel:(CancelBlock)cancelBlock
{
    self.completeBlock = completeBlock;
    self.cancelBlock = cancelBlock;
    
    // iOS版本
    CGFloat iOSVersion = [[[UIDevice currentDevice] systemVersion] floatValue];
    
    if (iOSVersion >= 8.0) { // iOS 8.0及以上
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
        UIAlertAction *cameraAction = [UIAlertAction actionWithTitle:@"拍照" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self openCamera];
        }];
        UIAlertAction *photoAction = [UIAlertAction actionWithTitle:@"本地相册" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self openPhoto];
        }];
        
        [alertController addAction:cancelAction];
        
        [alertController addAction:cameraAction];
        [alertController addAction:photoAction];
        [self presentViewController:alertController animated:YES completion:nil];
        
    } else { // iOSVersion < 8.0
        // 选择按钮控件
        UIActionSheet *myActionSheet = [[UIActionSheet alloc]
                                        initWithTitle:nil
                                        delegate:self
                                        cancelButtonTitle:@"取消"
                                        destructiveButtonTitle:nil
                                        otherButtonTitles:@"拍照", @"本地相册", nil];
        [myActionSheet showInView:self.view];
    }
}

/// 开始拍照
- (void)openCamera
{
    UIImagePickerControllerSourceType sourceType = UIImagePickerControllerSourceTypeCamera;
    // 设备是否支持使用摄像头
    BOOL isSupport = [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera];
    if (isSupport)
    {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        // 设置拍照后的图片可被编辑
        picker.allowsEditing = YES;
        picker.sourceType = sourceType;
        [self presentViewController:picker animated:YES completion:nil];
    }
    else {
        NSLog(@"模拟器中无法打开照相机,请在真机中使用");
    }
}

/// 打开本地相册
- (void)openPhoto
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    picker.delegate = self;
    //设置选择后的图片可被编辑
    picker.allowsEditing = YES;
    [self presentViewController:picker animated:YES completion:nil];
}


#pragma mark - UIActionSheetDelegate
//菜单按钮点击后的响应
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == actionSheet.cancelButtonIndex)   // 点击取消按钮
    {
        NSLog(@"取消");
        self.cancelBlock();
    }
    
    switch (buttonIndex)
    {
        case 0:  //打开照相机拍照
            [self openCamera];
            break;
            
        case 1:  //打开本地相册
            [self openPhoto];
            break;
            
        default:
            
            break;
    }
}

#pragma mark - UIImagePickerControllerDelegate
// 当选择一张图片后进入这里
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSLog(@"%@", info);
    UIImage *_theImage;
    NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    
    // 当选择的类型是图片 (public.image)
    if ([mediaType isEqualToString:@"public.image"]) {
        
        _theImage = [info objectForKey:UIImagePickerControllerEditedImage];
//        data = UIImageJPEGRepresentation(_theImage, 1);
        
        
        // UIImagePickerControllerOriginalImage 原始图片
        // UIImagePickerControllerEditedImage 编辑后图片
        
        // fileName
//        NSURL *imageURL = [info valueForKey:UIImagePickerControllerReferenceURL];
        
//        ALAsset asset = [];
        
//        ALAssetsLibraryAssetForURLResultBlock resultblock = ^(ALAsset *myasset)
//        {
//            ALAssetRepresentation *representation = [myasset defaultRepresentation];
//            NSString *fileName = [representation filename];
//            NSLog(@"fileName : %@",fileName);
//        };
        
        self.completeBlock(_theImage, nil);
        
        // 关闭相册界面
        [picker dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    // 关闭相册界面
    [self dismissViewControllerAnimated:YES completion:nil];
    if (self.cancelBlock) {
        self.cancelBlock();
    }
}

@end
