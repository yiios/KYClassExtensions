//
//  UIViewController+GetLocation.m
//  SZESS1217
//
//  Created by 魏信洋 on 16/1/10.
//  Copyright © 2016年 shanreal. All rights reserved.
//

#import "UIViewController+GetLocation.h"
#import <objc/runtime.h>
#import <CoreLocation/CoreLocation.h>

#define kIOS8_OR_LATER ([[[UIDevice currentDevice] systemVersion] compare:@"8.0"] != NSOrderedAscending)

static const char *LocationManagerKey = "LocationManagerKey";
static const char *GeocoderKey = "GeocoderKey";
static const char *CompleteBlockKey = "CompleteBlockKey";

@interface UIViewController ()<CLLocationManagerDelegate>

@property (nonatomic,strong) CLLocationManager *locationManager;
@property (nonatomic,strong) CLGeocoder *geocoder;

@end

@implementation UIViewController (GetLocation)

- (GetLocationStr)getLocationStr{
    return (GetLocationStr)objc_getAssociatedObject(self, CompleteBlockKey);
}
- (void)setGetLocationStr:(GetLocationStr)getLocationStr{
    objc_setAssociatedObject(self, CompleteBlockKey, getLocationStr, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (CLLocationManager *)locationManager{
    return (CLLocationManager *)objc_getAssociatedObject(self, LocationManagerKey);
}
- (void)setLocationManager:(CLLocationManager *)locationManager{
    objc_setAssociatedObject(self, LocationManagerKey, locationManager, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (CLGeocoder *)geocoder{
    return (CLGeocoder *)objc_getAssociatedObject(self, GeocoderKey);
}
- (void)setGeocoder:(CLGeocoder *)geocoder{
    objc_setAssociatedObject(self, GeocoderKey, geocoder, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (void)getLocationStrWithComplete:(GetLocationStr)complete
{
 
    self.getLocationStr = [complete copy];
    self.locationManager = [[CLLocationManager alloc] init];//创建位置管理器
    self.geocoder = [CLGeocoder new];
    if (kIOS8_OR_LATER) {
        
        //使用期间
        [self.locationManager requestWhenInUseAuthorization];
        //始终
        //or [self.locationManage requestAlwaysAuthorization]
    }
    
    //如果没有授权则请求用户授权
    if ([CLLocationManager authorizationStatus]==kCLAuthorizationStatusNotDetermined){
        [self.locationManager requestWhenInUseAuthorization];
        
    }else if([CLLocationManager authorizationStatus]==kCLAuthorizationStatusAuthorizedWhenInUse){
        //设置代理
        self.locationManager.delegate=self;
        //设置定位精度
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        //定位频率,每隔多少米定位一次
        CLLocationDistance distance=10.0;//十米定位一次
        self.locationManager.distanceFilter=distance;
        //启动跟踪定位
        [self.locationManager startUpdatingLocation];
    }
    
    [self.locationManager startUpdatingLocation];//启动位置管理器
    
    if (![CLLocationManager locationServicesEnabled]) {
        NSLog(@"定位服务当前可能尚未打开，请设置打开！");
        return;
    }

    
}



#pragma mark - CLLocationManagerDelegate
-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations
{
    [self.locationManager stopUpdatingLocation];
    
    CLLocation *userLocation = [locations firstObject];
    
    [self.geocoder reverseGeocodeLocation:userLocation completionHandler:^(NSArray *placemarks, NSError *error) {
        CLPlacemark *placemark=[placemarks firstObject];
        NSLog(@"loc-详细信息[name]:%@",placemark.addressDictionary[@"Name"]);

        if (self.getLocationStr) {
            self.getLocationStr(placemark.addressDictionary[@"Name"]);
        }
        
    }];
    
    
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    
    if (
        ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)] && status != kCLAuthorizationStatusNotDetermined && status != kCLAuthorizationStatusAuthorizedWhenInUse) ||
        (![self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)] && status != kCLAuthorizationStatusNotDetermined && status != kCLAuthorizationStatusAuthorized)
        ) {
        
        
    }else {
        
        [self.locationManager startUpdatingLocation];
    }
}



#pragma mark - 添加或者删除视图控制器
- (void)wk_addChildViewController:(UIViewController *)childController
{
    [self addChildViewController:childController];
    [self.view addSubview:childController.view];
    [childController didMoveToParentViewController:self];
}

- (void)wk_removeFromParentViewController
{
    [self willMoveToParentViewController:nil];
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
}


@end
