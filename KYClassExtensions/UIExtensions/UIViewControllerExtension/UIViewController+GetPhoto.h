//
//  UIViewController+GetPhoto.h
//
//  Copyright © 2015年 kylin. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^CompleteBlock)(UIImage *image, NSString *imageName);
typedef void(^CancelBlock)(void);

@interface UIViewController (GetPhoto)

@property (nonatomic, strong) CompleteBlock completeBlock;
@property (nonatomic, strong) CancelBlock cancelBlock;

- (void)chooseImageWithComplete:(CompleteBlock)completeBlock Cancel:(CancelBlock)cancelBlock;

@end
