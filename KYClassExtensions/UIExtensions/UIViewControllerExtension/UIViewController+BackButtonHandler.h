//
//  UIViewController+BackButtonHandler.h
//  kyExpress
//
//  Created by zhigang on 16/3/5.
//  Copyright © 2016年 kyExpress. All rights reserved.
//

//#import <UIKit/UIKit.h>
//
//@interface UIViewController (BackButtonHandler)
//
//@optional
//
//// Override this method in UIViewController derived class to handle 'Back' button click
//
//-(BOOL)navigationShouldPopOnBackButton;
//
//@end
//
//@interface UIViewController (BackButtonHandler) <BackButtonHandlerProtocol>
//
//@end


#import <UIKit/UIKit.h>

@protocol BackButtonHandlerProtocol <NSObject>

@optional

// Override this method in UIViewController derived class to handle 'Back' button click

-(BOOL)navigationShouldPopOnBackButton;

@end

@interface UIViewController (BackButtonHandler) <BackButtonHandlerProtocol>

@end
