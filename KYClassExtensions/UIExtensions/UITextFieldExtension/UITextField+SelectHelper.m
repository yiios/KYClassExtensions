//
//  UITextField+SelectHelper.m
//  ClassExtensions
//
//  Created by zhigang on 17/4/17.
//  Copyright © 2017年 King. All rights reserved.
//

#import "UITextField+SelectHelper.h"

@implementation UITextField (SelectHelper)

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender {
    
    UIPasteboard *board = [UIPasteboard generalPasteboard];
    
    //如果没有选中的文本
    if (self.selectedTextRange.isEmpty) {
        if (board.string &&[self.text isEqualToString:@""]) {
            //开启1个正常模式：粘贴
            if((action == @selector(paste:))&&!self.secureTextEntry){
                
                return YES;
                
            }
            
        }else if(board.string && ![self.text isEqualToString:@""]){
            //开启3个正常模式：全选，选择，粘贴
            if((action == @selector(selectAll:)||action == @selector(select:)||action == @selector(paste:))&&!self.secureTextEntry){
                
                return YES;
                
            }
            
        }else if(!board.string && ([self.text isEqualToString:@""])){
            //开启0个正常模式：
            if(!self.secureTextEntry){
                
                //return YES;
                
            }
            
        }else{
            //开启2个正常模式：全选，选择
            if((action == @selector(selectAll:)||action == @selector(select:))&&!self.secureTextEntry){
                
                return YES;
                
            }
            
        }
        
    }else{
        //如果有选中的文本
        /**
         *  _transliterateChinese 为系统私有方法
         */
        if (board.string) {
            //开启1个正常模式：粘贴
            if((action == @selector(copy:)||action == @selector(cut:)||action == @selector(paste:)||action == @selector(_transliterateChinese:))&&!self.secureTextEntry){
                
                return YES;
                
            }
            
        }else{
            if((action == @selector(copy:)||action == @selector(cut:)||action == @selector(_transliterateChinese:))&&!self.secureTextEntry){
                
                return YES;
                
            }
        }
    }
    
    //密文模式不能拷贝与剪切
    if(self.secureTextEntry ==YES){
        if(action == @selector(copy:)||action == @selector(cut:)){
            
            return NO;
        }
        if(action == @selector(selectAll:)||action == @selector(select:)||action == @selector(paste:)){
            
            return NO;
        }
    }
    
    //其余全部关闭
    return NO;
}

- (BOOL)canBecomeFirstResponder{
    return YES;
}

@end
