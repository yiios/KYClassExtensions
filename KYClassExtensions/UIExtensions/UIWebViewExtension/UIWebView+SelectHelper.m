//
//  UIWebView+SelectHelper.m
//  ClassExtensions
//
//  Created by zhigang on 17/4/17.
//  Copyright © 2017年 King. All rights reserved.
//

#import "UIWebView+SelectHelper.h"

@implementation UIWebView (SelectHelper)

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender {
    
    //开启5个正常模式：拷贝，剪切，全选，选择，粘贴
    if((action == @selector(copy:)||action == @selector(selectAll:)||action == @selector(select:))){
        
        return YES;
        
    }
    //其余全部关闭
    return NO;
}


@end
