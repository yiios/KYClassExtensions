//
//  UITableViewCell+Common.m
//  kyExpress
//
//  Created by 陈志刚 on 15/12/6.
//  Copyright © 2015年 kyExpress. All rights reserved.
//

#import "UITableViewCell+Common.h"

#import "UIColor+HexString.h"

@implementation UITableViewCell (Common)

- (void)setTextLabelFontAndColor{
    self.textLabel.font = [UIFont systemFontOfSize:16.0f];
    self.textLabel.textColor = [UIColor colorWithHexString:@"333333"] ;
}

@end
