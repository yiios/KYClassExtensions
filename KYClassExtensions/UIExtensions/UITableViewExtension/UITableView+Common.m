//
//  UITableView+Common.m
//  kyExpress
//
//  Created by 陈志刚 on 15/12/8.
//  Copyright © 2015年 kyExpress. All rights reserved.
//

#import "UITableView+Common.h"
#import <objc/runtime.h>
#import "UIView+Radius.h"
#import "UIView+MJExtension.h"
#import "UIImage+Common.h"

static const char *sureButtonnagerKey = "sureButtonnagerKey";

@implementation UITableView (Common)

- (UIButton *)sureButton{
    return objc_getAssociatedObject(self, sureButtonnagerKey);
}

- (void)setSureButton:(UIButton *)sureButton{
    objc_setAssociatedObject(self, sureButtonnagerKey, sureButton, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (UIView *)tableViewToFootViewWithTitle:(NSString *)title buttonBackgroundColor:(UIColor *)color handler:(void (^)(id sender))handler {
    
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 60)];
    bgView.backgroundColor = [UIColor clearColor];
    
    self.sureButton= [UIButton buttonWithType:UIButtonTypeCustom];
    self.sureButton.frame = CGRectMake(25, 5, [UIScreen mainScreen].bounds.size.width - 60, 44);
    [self.sureButton setTitle:title forState:UIControlStateNormal];
//    [button setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
    [self.sureButton setBackgroundImage:[UIImage imageWithColor:color] forState:UIControlStateNormal];
    [self.sureButton setBackgroundImage:[UIImage imageWithColor:color] forState:UIControlStateHighlighted];
    
//    [self.sureButton setBackgroundImage:[UIImage imageWithColor:[UIColor lightGrayColor]] forState:UIControlStateHighlighted];

    self.sureButton.backgroundColor = color;
    [self.sureButton cornerRadiusToNumber:6.0];
    [bgView addSubview:self.sureButton];
    
    [self.sureButton bk_addEventHandler:handler forControlEvents:UIControlEventTouchUpInside];
    
    return bgView;
    
}
- (UIView *)tableViewToFootViewWithTitle:(NSString *)title buttonChangeY:(CGFloat)buttonChangeY buttonBackgroundColor:(UIColor *)color handler:(void (^)(id sender))handler {
    
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 60+buttonChangeY)];
    bgView.backgroundColor = [UIColor clearColor];
    
    self.sureButton= [UIButton buttonWithType:UIButtonTypeCustom];
    self.sureButton.frame = CGRectMake(25, (bgView.mj_h - 44)*0.5, [UIScreen mainScreen].bounds.size.width - 50, 44);
    [self.sureButton setTitle:title forState:UIControlStateNormal];
    //    [button setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
    [self.sureButton setBackgroundImage:[UIImage imageWithColor:color] forState:UIControlStateNormal];
    [self.sureButton setBackgroundImage:[UIImage imageWithColor:color] forState:UIControlStateHighlighted];
    
    //    [self.sureButton setBackgroundImage:[UIImage imageWithColor:[UIColor lightGrayColor]] forState:UIControlStateHighlighted];
    
    self.sureButton.backgroundColor = color;
    [self.sureButton cornerRadiusToNumber:5];
    [bgView addSubview:self.sureButton];
    
    [self.sureButton bk_addEventHandler:handler forControlEvents:UIControlEventTouchUpInside];
    
    return bgView;
    
}

 - (UIView *)yp_tableViewToFootViewWithTitle:(NSString *)title buttonNomalBackgroundColor:(UIColor *)nomalColor buttonHighlightedBackgroundColor:(UIColor *)highlightColor handler:(void (^)(id sender))handler {
 
 UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 60)];
 bgView.backgroundColor = [UIColor clearColor];
 
 self.sureButton= [UIButton buttonWithType:UIButtonTypeCustom];
 self.sureButton.frame = CGRectMake(26, 10, [UIScreen mainScreen].bounds.size.width - 52, 40);
 [self.sureButton setTitle:title forState:UIControlStateNormal];
 //    [button setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
 [self.sureButton setBackgroundImage:[UIImage imageWithColor:nomalColor] forState:UIControlStateNormal];
 [self.sureButton setBackgroundImage:[UIImage imageWithColor:highlightColor] forState:UIControlStateHighlighted];
 
 //    [self.sureButton setBackgroundImage:[UIImage imageWithColor:[UIColor lightGrayColor]] forState:UIControlStateHighlighted];
 
 self.sureButton.backgroundColor = nomalColor;
 [self.sureButton cornerRadiusToNumber:5];
 [bgView addSubview:self.sureButton];
 
 [self.sureButton bk_addEventHandler:handler forControlEvents:UIControlEventTouchUpInside];
 
 return bgView;
 
 }



- (void)setSureButtonBackgroundImageWithColor:(UIColor *)color{
    [self.sureButton setBackgroundImage:[UIImage imageWithColor:color] forState:UIControlStateNormal];
}


@end
