//
//  UITableView+Common.h
//  kyExpress
//
//  Created by 陈志刚 on 15/12/8.
//  Copyright © 2015年 kyExpress. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIControl+BlocksKit.h"

@interface UITableView (Common)

@property (nonatomic, strong) UIButton *sureButton;
- (UIView *)tableViewToFootViewWithTitle:(NSString *)title buttonBackgroundColor:(UIColor *)color handler:(void (^)(id sender))handler;
- (UIView *)tableViewToFootViewWithTitle:(NSString *)title buttonChangeY:(CGFloat)buttonChangeY buttonBackgroundColor:(UIColor *)color handler:(void (^)(id sender))handler ;
- (void)setSureButtonBackgroundImageWithColor:(UIColor *)color;

- (UIView *)yp_tableViewToFootViewWithTitle:(NSString *)title buttonNomalBackgroundColor:(UIColor *)nomalColor buttonHighlightedBackgroundColor:(UIColor *)highlightColor handler:(void (^)(id sender))handler ;
@end
