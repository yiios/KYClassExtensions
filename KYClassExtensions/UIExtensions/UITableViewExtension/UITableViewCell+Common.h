//
//  UITableViewCell+Common.h
//  kyExpress
//
//  Created by 陈志刚 on 15/12/6.
//  Copyright © 2015年 kyExpress. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITableViewCell (Common)

- (void)setTextLabelFontAndColor;

@end
