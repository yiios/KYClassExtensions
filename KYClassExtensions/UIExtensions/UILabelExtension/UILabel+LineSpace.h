//
//  UILabel+LineSpace.h
//  kyExpress_Internal
//
//  Created by limo on 2017/3/23.
//  Copyright © 2017年 kyExpress. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (LineSpace)

//请不要将属性名称取为lineSpacing，UILabel已有此隐藏属性。
@property (assign, nonatomic) CGFloat lineSpace;

@end
