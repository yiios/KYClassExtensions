//
//  UILabel+Ky_MyExpreeLabelCategory.h
//  kyExpress_Internal
//
//  Created by alangavin on 2016/11/9.
//  Copyright © 2016年 kyExpress. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (Ky_MyExpreeLabelCategory)
- (CGSize)boundingRectWithSize:(CGSize)size;

@end
