//
//  UILabel+Common.m
//  kyExpress_Internal
//
//  Created by iOS_Chris on 17/3/7.
//  Copyright © 2017年 kyExpress. All rights reserved.
//

#import "UILabel+Common.h"
#import <CoreText/CoreText.h>

@implementation UILabel (Common)

//字数计算
- (void)msCountStyle{
    
    self.textColor = [UIColor colorWithRed:(double)224/255.0f green:(double)93/255.0f blue:(double)64/255.0f alpha:1];
    self.font = [UIFont systemFontOfSize:14.f];
    self.textAlignment = NSTextAlignmentRight;
    
}

//占位符
- (void)msPlaceHolderStyle{
    
    self.textColor = [UIColor colorWithRed:(double)196/255.0f green:(double)195/255.0f blue:(double)199/255.0f alpha:1];
    self.numberOfLines = 0;
    self.lineBreakMode = NSLineBreakByWordWrapping;
    
    self.font = [UIFont systemFontOfSize:14.f];
    self.textAlignment = NSTextAlignmentRight;
    
}


- (void)textAlignmentLeftAndRightWithWordCountEqualFour{
    
    if ([self labelWordExceededCount:4] == YES) {
        self.text = [self.text stringByReplacingOccurrencesOfString:@" " withString:@""];;
    } else {
        return;
    }
    
    // 获得标准宽度
    CGFloat labelWidth = [self getWidthWithWordCount:4];
    // 根据标准宽度获取实际的宽度
    CGSize size = [self.text boundingRectWithSize:CGSizeMake(labelWidth,MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingTruncatesLastVisibleLine|NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:self.font} context:nil].size;
    
    // 获得label文字的数量(剔除空格的)
    NSInteger length = (self.text.length-1);
    NSString* lastStr = [self.text substringWithRange:NSMakeRange(length,1)];
    if([lastStr isEqualToString:@":"]||[lastStr isEqualToString:@"："]) {
        length = (self.text.length-2);
    }
    
    CGFloat margin = (labelWidth - size.width)/length;
    
    NSNumber *number = [NSNumber numberWithFloat:margin];
    
    NSMutableAttributedString* attribute = [[NSMutableAttributedString alloc] initWithString:self.text];
    [attribute addAttribute:NSKernAttributeName value:number range:NSMakeRange(0,length)];
    
    self.attributedText = attribute;
    
}

- (BOOL)labelWordExceededCount:(NSUInteger)wordCount {
    if (self.text == nil || self.text == NULL) {
        return NO;
    } else if ([self.text isKindOfClass:[NSNull class]]) {
        return NO;
    } else if ([self.text isKindOfClass:[NSString class]] == NO) {
        return NO;
    } else if ([self.text isEqualToString:@"null"]) {
        return NO;
    } else if ([self.text isEqualToString:@"(null)"]) {
        return NO;
    } else if ([self.text isEqualToString:@""]) {
        return NO;
    }
    
    // 剔除多余的空格键和冒号之后看 label 的字数是否超标
    NSString *labelText = [self.text stringByReplacingOccurrencesOfString:@" " withString:@""];;
    NSString *lastStr = [labelText substringWithRange:NSMakeRange(labelText.length-1,1)];
    if ([lastStr isEqualToString:@":"]||[lastStr isEqualToString:@"："]) {
        labelText = [labelText substringToIndex:([labelText length]-1)];
    }
    if (labelText.length <= wordCount) {
        return YES;
    } else {
        return NO;
    }
}


//用对象的方法计算文本的大小
- (CGFloat)getWidthWithWordCount:(NSUInteger)wordCount {
    
    //返回一个矩形，大小等于文本绘制完占据的宽和高。
    NSString *tempStr = @"";
    for (NSUInteger i = 0; i<wordCount; i++) {
        tempStr = [tempStr stringByAppendingFormat:@"圈"];
    }
    NSString *labelStr = self.text;
    NSString *lastStr = [labelStr substringWithRange:NSMakeRange(labelStr.length-1,1)];
    if ([lastStr isEqualToString:@":"]||[lastStr isEqualToString:@"："]) {
        tempStr = [tempStr stringByAppendingFormat:@"："];
    }
    
    //特殊的格式要求都写在属性字典中
    CGSize strSize = [tempStr boundingRectWithSize:CGSizeMake([UIScreen mainScreen].bounds.size.width,CGFLOAT_MAX)  options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:self.font} context:nil].size;
    return strSize.width;
}

@end
