//
//  UILabel+Common.h
//  kyExpress_Internal
//
//  Created by iOS_Chris on 17/3/7.
//  Copyright © 2017年 kyExpress. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (Common)

//字数计算
- (void)msCountStyle;
//占位符
- (void)msPlaceHolderStyle;

/**
 UILabel 两端对齐 (适用于四个字以上的情况)
 */
- (void)textAlignmentLeftAndRightWithWordCountEqualFour;


@end
