//
//  UILabel+Ky_MyExpreeLabelCategory.m
//  kyExpress_Internal
//
//  Created by alangavin on 2016/11/9.
//  Copyright © 2016年 kyExpress. All rights reserved.
//

#import "UILabel+Ky_MyExpreeLabelCategory.h"

@implementation UILabel (Ky_MyExpreeLabelCategory)


- (CGSize)boundingRectWithSize:(CGSize)size
{
    NSDictionary *attribute = @{NSFontAttributeName: self.font};
    
    CGSize retSize = [self.text boundingRectWithSize:size
                                             options:\
                      NSStringDrawingTruncatesLastVisibleLine |
                      NSStringDrawingUsesLineFragmentOrigin |
                      NSStringDrawingUsesFontLeading
                                          attributes:attribute
                                             context:nil].size;
    
    return retSize;
}

@end
