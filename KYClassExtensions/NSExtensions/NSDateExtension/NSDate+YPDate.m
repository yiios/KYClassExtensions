//
//  NSDate+YPDate.m
//  kyExpress_Internal
//
//  Created by yeyongping on 2016/10/24.
//  Copyright © 2016年 kyExpress. All rights reserved.
//

#import "NSDate+YPDate.h"

@implementation NSDate (YPDate)

#pragma mark - NSDate 转 NSString
//yyyy-MM-dd HH:mm:ss
-(NSString *)toStringWithFormatter:(NSString *)formatter{
    NSDateFormatter *dataFormatter = [[NSDateFormatter alloc]init];
    dataFormatter.dateFormat = formatter;
    return [dataFormatter stringFromDate:self];
}

@end
