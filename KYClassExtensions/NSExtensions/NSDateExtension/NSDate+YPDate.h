//
//  NSDate+YPDate.h
//  kyExpress_Internal
//
//  Created by yeyongping on 2016/10/24.
//  Copyright © 2016年 kyExpress. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (YPDate)

#pragma mark - NSDate 转 NSString
//yyyy-MM-dd HH:mm:ss
-(NSString *)toStringWithFormatter:(NSString *)formatter;

@end
