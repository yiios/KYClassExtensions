//
//  NSArray+YPArray.m
//  kyExpress_Internal
//
//  Created by yeyongping on 2016/10/24.
//  Copyright © 2016年 kyExpress. All rights reserved.
//

#import "NSArray+YPArray.h"

@implementation NSArray (YPArray)

-(NSArray<UILabel *>*)creatLabelsFromStrings{
    NSMutableArray *arrM = [NSMutableArray array];
    [self enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj isKindOfClass:[NSString class]]) {
            UILabel *label = [[UILabel alloc]init];
            label.text = self[idx];
        }
        if ([obj isKindOfClass:[NSAttributedString class]]) {
            UILabel *label = [[UILabel alloc]init];
            label.attributedText = self[idx];
        }
    }];
    return arrM.copy;
}

@end
