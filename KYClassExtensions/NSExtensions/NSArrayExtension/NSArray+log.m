//
//  NSArray+log.m
//  categoryXG
//
//  Created by xiegang on 2017/4/25.
//  Copyright © 2017年 xiegang. All rights reserved.
//

#import "NSArray+log.h"

@implementation NSArray (log)

/*** 调整数组打印格式 */
- (NSString *)descriptionWithLocale:(id)locale
{
    NSMutableString *strM = [NSMutableString stringWithString:@"(\n"];
    
    [self enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [strM appendFormat:@"\t%@,\n", obj];
    }];
    
    [strM appendString:@")"];
    
    return strM;
}

@end
