//
//  NSArray+DataProcessing.h
//  ClassExtensions
//
//  Created by zhigang on 17/4/18.
//  Copyright © 2017年 King. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (DataProcessing)

/**
 * 返回删除数组中最后一个对象的数组
 *
 * Return the array obtained by removing the last object
 *
 */
- (NSArray *)arrayByRemovingLastObject;

@end
