//
//  NSArray+YPArray.h
//  kyExpress_Internal
//
//  Created by yeyongping on 2016/10/24.
//  Copyright © 2016年 kyExpress. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface NSArray (YPArray)

-(NSArray<UILabel *>*)creatLabelsFromStrings;

@end
