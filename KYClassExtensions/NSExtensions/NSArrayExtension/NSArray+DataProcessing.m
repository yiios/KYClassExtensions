//
//  NSArray+DataProcessing.m
//  ClassExtensions
//
//  Created by zhigang on 17/4/18.
//  Copyright © 2017年 King. All rights reserved.
//

#import "NSArray+DataProcessing.h"

@implementation NSArray (DataProcessing)

- (NSArray *)arrayByRemovingLastObject
{
    NSMutableArray *array = [self mutableCopy];
    [array removeLastObject];
    return [array copy];
}

@end
