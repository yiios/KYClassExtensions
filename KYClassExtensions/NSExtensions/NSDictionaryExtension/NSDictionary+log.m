//
//  NSDictionary+log.m
//  categoryXG
//
//  Created by xiegang on 2017/4/25.
//  Copyright © 2017年 xiegang. All rights reserved.
//

#import "NSDictionary+log.h"

@implementation NSDictionary (log)

/*** 调整数组打印格式 */
- (NSString *)descriptionWithLocale:(id)locale
{
    NSMutableString *string = [NSMutableString string];
    
    //    NSLog(@"locale : %@",locale);
    //
    //    NSLog(@"class : %@",[self class]);
    
    if ([self isKindOfClass:NSClassFromString(@"__NSCFDictionary")]){
        return @"";
    }
    
    //    NSLog(@"allKeys : %@ , allValues : %@",self.allKeys,self.allValues);
    
    // 开头有个{
    [string appendString:@"{\n"];
    
    // 遍历所有的键值对
    [self enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        [string appendFormat:@"\t%@", key];
        [string appendString:@" : "];
        [string appendFormat:@"%@,\n", obj];
    }];
    
    // 结尾有个}
    [string appendString:@"}"];
    
    // 查找最后一个逗号
    NSRange range = [string rangeOfString:@"," options:NSBackwardsSearch];
    if (range.location != NSNotFound)
        [string deleteCharactersInRange:range];
    
    return string;
}
@end
