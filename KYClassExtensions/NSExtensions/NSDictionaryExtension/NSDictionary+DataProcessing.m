//
//  NSDictionary+DataProcessing.m
//  ClassExtensions
//
//  Created by zhigang on 17/4/18.
//  Copyright © 2017年 King. All rights reserved.
//

#import "NSDictionary+DataProcessing.h"

@implementation NSDictionary (DataProcessing)

- (NSDictionary *)dictionaryByRemovingObjectForKey:(id)key
{
    NSMutableDictionary *dictionary = [self mutableCopy];
    [dictionary removeObjectForKey:key];
    return [dictionary copy];
}


- (NSDictionary *)dictionaryByRemovingObjectsForKeys:(NSArray *)keyArray{
    
    NSMutableDictionary *dictionary = [self mutableCopy];
    [dictionary removeObjectsForKeys:keyArray];
    return [dictionary copy];
}

@end
