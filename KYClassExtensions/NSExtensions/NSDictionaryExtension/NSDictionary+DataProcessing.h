//
//  NSDictionary+DataProcessing.h
//  ClassExtensions
//
//  Created by zhigang on 17/4/18.
//  Copyright © 2017年 King. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (DataProcessing)

/**
 * Return the receiver without the object designated by key
 *
 * 根据key 删除字典中属性
 */
- (NSDictionary *)dictionaryByRemovingObjectForKey:(id)key;

/**
 * Return the receiver without the objects designated by the keys and the array
 *
 * 根据keys 删除字典中属性
 */
- (NSDictionary *)dictionaryByRemovingObjectsForKeys:(NSArray *)keyArray;

@end
