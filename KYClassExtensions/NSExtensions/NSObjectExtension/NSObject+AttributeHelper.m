//
//  NSObject+AttributeHelper.m
//  ClassExtensions
//
//  Created by zhigang on 17/4/18.
//  Copyright © 2017年 King. All rights reserved.
//

#import "NSObject+AttributeHelper.h"
#import <objc/runtime.h>

@implementation NSObject (AttributeHelper)

+ (NSString *)className
{
    return NSStringFromClass(self);
}

- (NSString *)className
{
    return @(class_getName([self class]));
}

@end
