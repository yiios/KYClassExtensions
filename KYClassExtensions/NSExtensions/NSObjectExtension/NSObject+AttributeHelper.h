//
//  NSObject+AttributeHelper.h
//  ClassExtensions
//
//  Created by zhigang on 17/4/18.
//  Copyright © 2017年 King. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (AttributeHelper)

/**
 * Return the name of a class as a string
 */
+ (NSString *)className;

/**
 * Return the name of an object's class (as returned by [self class]) as a string. May be faked by dynamic subclasses
 * (e.g. those added by KVO)
 */
@property (nonatomic, readonly, copy) NSString *className;

@end
