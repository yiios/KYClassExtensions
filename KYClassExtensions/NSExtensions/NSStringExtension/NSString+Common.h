//
//  NSString+Common.h
//  King
//
//  Created by King on 15/6/9.
//  Copyright (c) 2015年 c521xiong. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface NSString (Common)

/**
 *  转换拼音
 *
 *  @return 拼音
 */
- (NSString *)transformToPinyin;

/**
 *  判断是否为整形
 *
 *  @return yes no
 */
- (BOOL)isPureInt;
/**
 *  判断是否为浮点形
 *
 *  @return yes no
 */
- (BOOL)isPureFloat;
/**
 *  是否为空
 *
 *  @return yes no
 */
- (BOOL)isEmpty;

/**
 *	@brief	返回是否是电话号码
 *
 *	@param 	mobileNum 	传入的电话号码
 *
 *	@return	返回判断后的布尔值
 */
- (BOOL)isMobileNum;

/**
 *	@brief	利用正则表达式验证邮箱的合法性
 *
 *	@param 	email 	邮箱NSString
 *
 *	@return	是否合法
 */
- (BOOL)isValidateEmail;

/**
 *  MD5加密  32 位 小写
 *
 *  @return MD5 String
 */
- (NSString *)md5StrForLow;


/**
 *  MD5加密  32 位 大写
 *
 *  @return MD5 String
 */
- (NSString *)md5StrForUp;
+ (NSString*)getMD5WithData:(NSData *)data;
+ (NSString *) md5_base64WithData:(NSData *)data;

/**
 *  哈希
 */
- (NSString*)sha1Str;
/**
 *  获取沙盒路径
 *
 *  @return 返回沙盒路径
 */
+ (NSString *)getDocumentPath;

/**
 *  判断str是否包含foundStr字符串
 *
 *  @param str      原始字符串
 *  @param foundStr 待查找字符串
 *
 *  @return y or n
 */
+ (BOOL) isRangeOfString:(NSString *)str found:(NSString *)foundStr;

//根据文字个数自动匹配高度
- (CGRect)textRectWithSize:(CGSize)size attributes:(NSDictionary *)attributes;

- (NSString *)subStringFrom:(NSString *)startString to:(NSString *)endString;



@end
