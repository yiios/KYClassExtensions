//
//  NSString+Encryption.h
//  ClassExtensions
//
//  Created by zhigang on 17/4/17.
//  Copyright © 2017年 King. All rights reserved.
//

/*
    字符串加密分类
 */

#import <Foundation/Foundation.h>

@interface NSString (Encryption)

/**
 *  MD5加密  32 位 小写
 *
 *  @return MD5 String
 */
- (NSString *)md5StrForLow;


/**
 *  MD5加密  32 位 大写
 *
 *  @return MD5 String
 */
- (NSString *)md5StrForUp;

/**
 * SHA1编码
 *
 *  @return SHA1 String
 */
- (NSString *)sha1Encryption;

/**
 * SHA256加密方式
 *
 *  @return SHA256 String
 */
- (NSString *)sha256Encryption;


/**
 * SHA384加密方式
 *
 *  @return SHA384 String
 */
- (NSString *)sha384Encryption;

/**
 * SHA512加密方式
 *
 *  @return SHA512 String
 */
- (NSString *)sha512Encryption;


- (NSString *)md2hash;

- (NSString *)md4hash;

- (NSString *)md5hash;


@end
