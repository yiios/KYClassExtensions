//
//  NSString+CharacterProcessingHelper.h
//  ClassExtensions
//
//  Created by zhigang on 17/4/17.
//  Copyright © 2017年 King. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface NSString (CharacterProcessingHelper)

/**
 *  去除某个字符 
 *  @param aString 为需要去除的字符
 */
- (NSString *)removeQuotesString:(NSString *)aString;

/**
 *  返回带有删除线的字符串
 *  @return 带有删除线的字符串
 */
- (NSMutableAttributedString *)deleteTheLineOfString;

/**
 *  判断是否为空字符串
 */
- (BOOL)isEmpty;

/**
 *  Counts number of Words in String
 */
- (NSUInteger)countNumberOfWords;

/**
 * Get Substring from particular location to given lenght
 */
- (NSString*)getSubstringFrom:(NSInteger)begin to:(NSInteger)end;

/**
 * Add substring to main String
 */
- (NSString *)addString:(NSString *)string;

/**
 * Remove particular sub string from main string
 */
- (NSString *)removeSubString:(NSString *)subString;

+ (NSString *)getMyApplicationVersion;
+ (NSString *)getMyApplicationName;

/**
 * convertToData
 */
- (NSData *)convertToData;
/**
 * DataToString
 */
+ (NSString *)getStringFromData:(NSData *)data;
/**
 * 字符串转换富文本
 * pram color 要转换的颜色
 * pram keyWords  需要变色的子串集合
 */
-(NSAttributedString *)yp_color:(UIColor *)color keyWords:(NSArray *)keyWords;

/*** 过滤手机号 "-","（" ,"）"," " */
- (NSString*)initTelephoneWithReformat;

/** 根据文字个数自动匹配高度 */
- (CGRect)textRectWithSize:(CGSize)size attributes:(NSDictionary *)attributes;

@end
