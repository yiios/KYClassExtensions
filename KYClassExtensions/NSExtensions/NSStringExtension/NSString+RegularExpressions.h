//
//  NSString+RegularExpressions.h
//  ClassExtensions
//
//  Created by zhigang on 17/4/17.
//  Copyright © 2017年 King. All rights reserved.
//

/*  
    正则表达式分类
 */

#import <Foundation/Foundation.h>

@interface NSString (RegularExpressions)

/**
 *	@brief	返回是否是电话号码
 *
 *	@return	返回判断后的布尔值
 */
- (BOOL)isMobileNum;

/**
 *	@brief	利用正则表达式验证邮箱的合法性
 *
 *	@return	是否合法
 */
- (BOOL)isValidateEmail;

/**
 *	@brief	利用正则表达式验证微信号的合法性
 *
 *	@return	是否合法
 */
- (BOOL)isValidateWechatNumber;

/**
 *  是否包含中文
 */
- (BOOL)isContantChineseCharacters;

/**
 *	@brief	利用正则表达式验证是否中文
 *
 *	@return	是否合法
 */
- (BOOL)isValidateChineseCharacters;

/**
 *  判断是否只包含中文和英文
 */
- (BOOL)isValidateNumOrEnglish;

/**
 *  是否是0-9之间的数值
 */
- (BOOL)isZeroToNine;

/**
 *  是否是A-Z之间的大写字母
 */
- (BOOL)isAtoZ;

/**
 *  是否是a-z之间的小写字母
 */
- (BOOL)isaToz;

/**
 *  是否是由26个英文字母组成的字符串
 */
- (BOOL)isAatoZz;

/**
 * 是否是网址
 */
- (BOOL)isValidUrl;

/**
 *  判断是否为整形
 */
- (BOOL)isPureInt;

/**
 *  传入正则表达式验证
 */
- (BOOL)evaluateWithSString:(NSString *)regularString;


@end
