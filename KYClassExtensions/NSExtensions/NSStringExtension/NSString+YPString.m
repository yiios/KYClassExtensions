//
//  NSString+YPString.m
//  kyExpress_Internal
//
//  Created by yeyongping on 2016/10/24.
//  Copyright © 2016年 kyExpress. All rights reserved.
//

#import "NSString+YPString.h"

@implementation NSString (YPString)

#pragma mark - NSString 转 NSdate
//yyyy-MM-dd HH:mm:ss
-(NSDate *)toDateWithFormatter:(NSString *)formatter{
    NSDateFormatter *dataFormatter = [[NSDateFormatter alloc]init];
    dataFormatter.dateFormat = formatter;
    return [dataFormatter dateFromString:self];
}

#pragma mark - 转富文本
-(NSAttributedString *)yp_color:(UIColor *)color keyWords:(NSArray *)keyWords{
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:self];
    if (keyWords) {
        [keyWords enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            NSMutableString *tmpString=[NSMutableString stringWithString:self];
            NSRange range=[self rangeOfString:obj];
            NSInteger location=0;
            while (range.length>0) {
                [attString addAttribute:(NSString*)NSForegroundColorAttributeName value:color range:NSMakeRange(location+range.location, range.length)];
                location+=(range.location+range.length);
                NSString *tmp= [tmpString substringWithRange:NSMakeRange(range.location+range.length, self.length-location)];
                tmpString=[NSMutableString stringWithString:tmp];
                range=[tmp rangeOfString:obj];
            }
        }];
    }
    return attString;
}

#pragma mark - 判断是否全部是中文
-(BOOL)isChinese{
    NSString* chinese =@"^[\u4e00-\u9fa5]{0,}$";
    NSPredicate *chinesePredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",chinese];
    return [chinesePredicate evaluateWithObject:self];
}

#pragma mark - 判断是不是空字符串
- (BOOL)isBlankString{
    if (self == nil || self == NULL) { return YES; }
    if ([self isKindOfClass:[NSNull class]]) { return YES; }
    if ([self isEqualToString:@"null"]) {return YES;}
    if ([[self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length]==0) {return YES;}
    return NO;
}



@end
