//
//  NSString+YPAttributeString.m
//  YPAlertViewController
//
//  Created by yeyongping on 2016/10/17.
//  Copyright © 2016年 com.yeyongping@qq.com. All rights reserved.
//

#import "NSString+YPAttributeString.h"

@implementation NSString (YPAttributeString)

-(NSAttributedString *)yp_color:(UIColor *)color keyWords:(NSArray *)keyWords{
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:self];
    if (keyWords) {
        [keyWords enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            NSMutableString *tmpString=[NSMutableString stringWithString:self];
            NSRange range=[self rangeOfString:obj];
            NSInteger location=0;
            while (range.length>0) {
                [attString addAttribute:(NSString*)NSForegroundColorAttributeName value:color range:NSMakeRange(location+range.location, range.length)];
                location+=(range.location+range.length);
                NSString *tmp= [tmpString substringWithRange:NSMakeRange(range.location+range.length, self.length-location)];
                tmpString=[NSMutableString stringWithString:tmp];
                range=[tmp rangeOfString:obj];
            }
        }];
    }
    return attString;
}

@end
