//
//  NSString+YPAttributeString.h
//  YPAlertViewController
//
//  Created by yeyongping on 2016/10/17.
//  Copyright © 2016年 com.yeyongping@qq.com. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface NSString (YPAttributeString)

-(NSAttributedString *)yp_color:(UIColor *)color keyWords:(NSArray *)keyWords;

@end
