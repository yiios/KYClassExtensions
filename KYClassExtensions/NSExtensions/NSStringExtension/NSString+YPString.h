//
//  NSString+YPString.h
//  kyExpress_Internal
//
//  Created by yeyongping on 2016/10/24.
//  Copyright © 2016年 kyExpress. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface NSString (YPString)

#pragma mark - NSString 转 NSdate
-(NSDate *)toDateWithFormatter:(NSString *)formatter;

#pragma mark - 转富文本
-(NSAttributedString *)yp_color:(UIColor *)color keyWords:(NSArray *)keyWords;

#pragma mark - 判断是否全部是中文
-(BOOL)isChinese;

#pragma mark - 判断是不是空字符串
- (BOOL)isBlankString;

@end
