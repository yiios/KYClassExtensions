//
//  NSString+RegularExpressions.m
//  ClassExtensions
//
//  Created by zhigang on 17/4/17.
//  Copyright © 2017年 King. All rights reserved.
//

#import "NSString+RegularExpressions.h"

@implementation NSString (RegularExpressions)


- (BOOL)isMobileNum
{
    if([self isEmpty]){
        return NO;
    }
    
    if(self.length > 11){
        return NO;
    }
    
    /**
     * 手机号码
     * 移动：134[0-8],135,136,137,138,139,150,151,157,158,159,182,187,188
     * 联通：130,131,132,152,155,156,185,186
     * 电信：133,1349,153,180,189
     */
    NSString * MOBILE = @"^1(3[0-9]|5[0-35-9]|8[0235-9])\\d{8}$";
    /**
     * 中国移动：China Mobile
     * 134[0-8],135,136,137,138,139,150,151,157,158,159,182,187,188
     */
    NSString * CM = @"^1(34[0-8]|(3[5-9]|5[017-9]|8[278])\\d)\\d{7}$";
    /**
     * 中国联通：China Unicom
     * 130,131,132,152,155,156,185,186
     */
    NSString * CU = @"^1(3[0-2]|5[256]|8[56])\\d{8}$";
    /**
     * 中国电信：China Telecom
     * 133,134,153,180,189
     */
    NSString * CT = @"^1((33|53|8[09])[0-9]|349)\\d{7}$";
    
    /**
     * 大陆地区固话及小灵通
     * 区号：010,020,021,022,023,024,025,027,028,029
     * 号码：七位或八位
     */
    //NSString * PHS = @"^0(10|2[0-5789]|\\d{3})\\d{7,8}$";
    
    NSPredicate *regextestmobile = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", MOBILE];
    NSPredicate *regextestcm = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CM];
    NSPredicate *regextestcu = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CU];
    NSPredicate *regextestct = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CT];
    
    if (([regextestmobile evaluateWithObject:self] == YES) || ([regextestcm evaluateWithObject:self] == YES)
        || ([regextestct evaluateWithObject:self] == YES) || ([regextestcu evaluateWithObject:self] == YES))
    {
        return YES;
    }
    else{
        return NO;
    }
}

// 利用正则表达式验证邮箱的合法性
- (BOOL)isValidateEmail {
    
    return [self evaluateWithSString:@"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"];
}


// 微信号正则验证
- (BOOL)isValidateWechatNumber{
    
    return [self evaluateWithSString:@"^[a-zA-Z0-9_]+$"];

}

/**
 *  是否包含中文
 */
- (BOOL)isContantChineseCharacters{
    
    for (int i = 1; i <= self.length; i++) {
        NSRange range = NSMakeRange(self.length - i, 1);
        NSString *string = [self substringWithRange:range];
        if (string.isValidateChineseCharacters) {
            return YES;
        }
    }
    return NO;
    
}


// 是否是中文
- (BOOL)isValidateChineseCharacters
{
    
    if(self.isEmpty){
        return NO;
    }
    
    NSRange range = [self  rangeOfString:@" "];
    BOOL isFound = range.location != NSNotFound;
    
    NSString * string = @"";
    if (isFound) {
        string = [self stringByReplacingOccurrencesOfString:@" " withString:@""];
    }else{
        string = self;
    }
    
    return [self evaluateWithSString:@"^[\u4e00-\u9fa5]*$"];
    
}

/**
 *  判断是否只包含中文和英文
 */
- (BOOL)isValidateNumOrEnglish{
    
    return [self evaluateWithSString:@"^([A-Za-z]|[\u4E00-\u9FA5])+$"];
}

//是否是0-9之间的数值
- (BOOL)isZeroToNine{
    
    return [self evaluateWithSString:@"^[0-9]*$"];
    
}

/**
 *  是否是A-Z之间的大写字母
 */
- (BOOL)isAtoZ{
    
    return [self evaluateWithSString:@"^[A-Z]+$"];
}

/**
 *  是否是a-z之间的小写字母
 */
- (BOOL)isaToz{
    
    return [self evaluateWithSString:@"^[a-z]+$"];
}

/**
 *  是否是由26个英文字母组成的字符串
 */
- (BOOL)isAatoZz{
  
    return [self evaluateWithSString:@"^[A-Za-z]+$"];
}

/**
 * 是否是网址
 */
- (BOOL)isValidUrl
{
    return [self evaluateWithSString:@"(http|https)://((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+"];
}

//判断是否为整形
- (BOOL)isPureInt{
    NSScanner* scan = [NSScanner scannerWithString:self];
    int val;
    return[scan scanInt:&val] && [scan isAtEnd];
}


// 传入相应的正则表达式来验证
- (BOOL)evaluateWithSString:(NSString *)regularString{
    
    if(self.isEmpty){
        return NO;
    }
    return [[NSPredicate predicateWithFormat:@"SELF MATCHES %@", regularString] evaluateWithObject:self];
}

// 判断是否为空字符串
- (BOOL)isEmpty{
    
    if (self == nil || self == NULL) {
        return YES;
    }
    if ([self isKindOfClass:[NSNull class]]) {
        return YES;
    }
    if ([[self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length]==0) {
        return YES;
    }
    return NO;
}



@end
