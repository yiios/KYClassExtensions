//
//  NSString+CharacterProcessingHelper.m
//  ClassExtensions
//
//  Created by zhigang on 17/4/17.
//  Copyright © 2017年 King. All rights reserved.
//

#import "NSString+CharacterProcessingHelper.h"
#import <UIKit/UIKit.h>

@implementation NSString (CharacterProcessingHelper)

/**
 *  去除某个字符
 */
- (NSString *)removeQuotesString:(NSString *)aString{
    
    NSRange range = [self  rangeOfString:aString];
    BOOL isFound = (range.location != NSNotFound);
    
    if (isFound){
        return [self stringByReplacingOccurrencesOfString:aString withString:@""];
    }
    return self;
}

/**
 *  返回带有删除线的字符串
 *  @return 带有删除线的字符串
 */
- (NSMutableAttributedString *)deleteTheLineOfString{
    
    NSMutableAttributedString *attri = [[NSMutableAttributedString alloc] initWithString:self];
    [attri addAttribute:NSStrikethroughStyleAttributeName value:@(NSUnderlinePatternSolid | NSUnderlineStyleSingle) range:NSMakeRange(0, self.length)];
    [attri addAttribute:NSStrikethroughColorAttributeName value:[UIColor groupTableViewBackgroundColor] range:NSMakeRange(0, self.length)];
    return attri;
    
}

// 判断是否为空字符串
- (BOOL)isEmpty{
    
    if (self == nil || self == NULL) {
        return YES;
    }
    if ([self isKindOfClass:[NSNull class]]) {
        return YES;
    }
    if ([[self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length]==0) {
        return YES;
    }
    return NO;
}



// remove white spaces from String
- (NSString *)removeWhiteSpacesFromString{
    
    NSString *trimmedString = [self stringByTrimmingCharactersInSet:
                               [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    return trimmedString;
}

// Counts number of Words in String
- (NSUInteger)countNumberOfWords{
    
    NSScanner *scanner = [NSScanner scannerWithString:self];
    NSCharacterSet *whiteSpace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    
    NSUInteger count = 0;
    while ([scanner scanUpToCharactersFromSet: whiteSpace  intoString: nil]) {
        count++;
    }
    
    return count;
}


// Get Substring from particular location to given lenght
- (NSString*)getSubstringFrom:(NSInteger)begin to:(NSInteger)end{
    
    NSRange r;
    r.location = begin;
    r.length = end - begin;
    return [self substringWithRange:r];
}

// Add substring to main String
- (NSString *)addString:(NSString *)string{
    
    if(!string || string.length == 0)
        return self;
    
    return [self stringByAppendingString:string];
}

// Remove particular sub string from main string
-(NSString *)removeSubString:(NSString *)subString{
    
    if ([self containsString:subString])
    {
        NSRange range = [self rangeOfString:subString];
        return  [self stringByReplacingCharactersInRange:range withString:@""];
    }
    return self;
}

- (BOOL)containsString:(NSString *)subString{
    
    return ([self rangeOfString:subString].location == NSNotFound) ? NO : YES;
}

// Get My Application Version number
+ (NSString *)getMyApplicationVersion
{
    NSDictionary *info = [[NSBundle mainBundle] infoDictionary];
    NSString *version = [info objectForKey:@"CFBundleVersion"];
    return version;
}

// Get My Application name
+ (NSString *)getMyApplicationName
{
    NSDictionary *info = [[NSBundle mainBundle] infoDictionary];
    NSString *name = [info objectForKey:@"CFBundleDisplayName"];
    return name;
}


// Convert string to NSData
- (NSData *)convertToData
{
    return [self dataUsingEncoding:NSUTF8StringEncoding];
}

// Get String from NSData
+ (NSString *)getStringFromData:(NSData *)data
{
    return [[NSString alloc] initWithData:data
                                 encoding:NSUTF8StringEncoding];
}

//转换富文本
-(NSAttributedString *)yp_color:(UIColor *)color keyWords:(NSArray *)keyWords{
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:self];
    if (keyWords) {
        [keyWords enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            NSMutableString *tmpString=[NSMutableString stringWithString:self];
            NSRange range=[self rangeOfString:obj];
            NSInteger location=0;
            while (range.length>0) {
                [attString addAttribute:(NSString*)NSForegroundColorAttributeName value:color range:NSMakeRange(location+range.location, range.length)];
                location+=(range.location+range.length);
                NSString *tmp= [tmpString substringWithRange:NSMakeRange(range.location+range.length, self.length-location)];
                tmpString=[NSMutableString stringWithString:tmp];
                range=[tmp rangeOfString:obj];
            }
        }];
    }
    return attString;
}

/** 根据文字个数自动匹配高度 */
- (CGRect)textRectWithSize:(CGSize)size attributes:(NSDictionary *)attributes
{
    return [self boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil];
}

//过滤手机号
- (NSString*)initTelephoneWithReformat
{
    
    if ([self containsString:@"-"])
    {
        self = [self stringByReplacingOccurrencesOfString:@"-" withString:@""];
    }
    
    if ([self containsString:@" "])
    {
        self = [self stringByReplacingOccurrencesOfString:@" " withString:@""];
    }
    
    if ([self containsString:@"("])
    {
        self = [self stringByReplacingOccurrencesOfString:@"(" withString:@""];
    }
    
    if ([self containsString:@")"])
    {
        self = [self stringByReplacingOccurrencesOfString:@")" withString:@""];
    }
    
    return self;
}

@end
