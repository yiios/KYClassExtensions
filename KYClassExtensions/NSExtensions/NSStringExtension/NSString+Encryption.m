//
//  NSString+Encryption.m
//  ClassExtensions
//
//  Created by zhigang on 17/4/17.
//  Copyright © 2017年 King. All rights reserved.
//

#import "NSString+Encryption.h"
#import <CommonCrypto/CommonDigest.h>
#import <MobileCoreServices/MobileCoreServices.h>

static NSString* digest(NSString *string, unsigned char *(*cc_digest)(const void *, CC_LONG, unsigned char *), CC_LONG digestLength)
{
    // Hash calculation
    unsigned char md[digestLength];     // C99
    memset(md, 0, sizeof(md));
    const char *utf8str = string.UTF8String;
    cc_digest(utf8str, (CC_LONG)strlen(utf8str), md);
    
    // Hexadecimal representation
    NSMutableString *hexHash = [NSMutableString string];
    for (NSUInteger i = 0; i < sizeof(md); ++i) {
        [hexHash appendFormat:@"%02X", md[i]];
    }
    
    return hexHash.lowercaseString;
}

@implementation NSString (Encryption)

//MD5 32位  小写
- (NSString *)md5StrForLow
{
    const char *cStr = [self UTF8String];
    unsigned char result[16];
    CC_MD5(cStr, (CC_LONG)strlen(cStr), result); // This is the md5 call
    return [[NSString stringWithFormat:
             @"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
             result[0], result[1], result[2], result[3],
             result[4], result[5], result[6], result[7],
             result[8], result[9], result[10], result[11],
             result[12], result[13], result[14], result[15]
             ] lowercaseString];
}

//MD5 32位  大写
- (NSString *)md5StrForUp
{
    const char *cStr = [self UTF8String];
    unsigned char result[16];
    CC_MD5(cStr, (CC_LONG)strlen(cStr), result); // This is the md5 call
    return [[NSString stringWithFormat:
             @"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
             result[0], result[1], result[2], result[3],
             result[4], result[5], result[6], result[7],
             result[8], result[9], result[10], result[11],
             result[12], result[13], result[14], result[15]
             ] uppercaseString];
}

- (NSString *)md2hash
{
    return digest(self, CC_MD2, CC_MD2_DIGEST_LENGTH);
}

- (NSString *)md4hash
{
    return digest(self, CC_MD4, CC_MD4_DIGEST_LENGTH);
}

- (NSString *)md5hash
{
    return digest(self, CC_MD5, CC_MD5_DIGEST_LENGTH);
}


// SHA1编码
- (NSString*)sha1Encryption
{
   return digest(self, CC_SHA1, CC_SHA1_DIGEST_LENGTH);
}

// sha256加密方式
- (NSString *)sha256Encryption{
    
    return digest(self, CC_SHA256, CC_SHA384_DIGEST_LENGTH);
}

//sha384加密方式
- (NSString *)sha384Encryption{
    
   return digest(self, CC_SHA384, CC_SHA384_DIGEST_LENGTH);
}

//sha512加密方式
- (NSString *)sha512Encryption{
    
    return digest(self, CC_SHA512, CC_SHA512_DIGEST_LENGTH);
}



@end
