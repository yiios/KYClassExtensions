//
//  NSObject+LocationConversion.h
//  ClassExtensions
//
//  Created by zhigang on 17/4/17.
//  Copyright © 2017年 King. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface NSObject (LocationConversion)

//火星转百度
+ (CLLocation*)marslocationFromBaiduLocation:(CLLocation *)bdLocation;
+ (CLLocationCoordinate2D)marsCoordFromBaiduCoord:(CLLocationCoordinate2D)coord;
//百度转火星
+ (CLLocation*)baidulocationFromMarsLocation:(CLLocation *)marsLocation;
+ (CLLocationCoordinate2D)baiduCoordFromMarsCoord:(CLLocationCoordinate2D)coord;

@end
