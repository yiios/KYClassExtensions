//
//  NSObject+LocationConversion.m
//  ClassExtensions
//
//  Created by zhigang on 17/4/17.
//  Copyright © 2017年 King. All rights reserved.
//

#import "NSObject+LocationConversion.h"

//地理位置转化用常量
const double a = 6378245.0;
const double ee = 0.00669342162296594323;
const double x_pi = M_PI * 3000.0 / 180.0;

@implementation NSObject (LocationConversion)

#pragma mark - 百度转火星
void transform_baidu_to_mars(double bd_lat, double bd_lon, double *gg_lat, double *gg_lon)
{
    double x = bd_lon - 0.0065, y = bd_lat - 0.006;
    double z = sqrt(x * x + y * y) - 0.00002 * sin(y * x_pi);
    double theta = atan2(y, x) - 0.000003 * cos(x * x_pi);
    *gg_lon = z * cos(theta);
    *gg_lat = z * sin(theta);
}

//从百度坐标到火星坐标
+ (CLLocation*)marslocationFromBaiduLocation:(CLLocation *)bdLocation
{
    double lat = 0.0;
    double lng = 0.0;
    transform_baidu_to_mars(bdLocation.coordinate.latitude, bdLocation.coordinate.longitude, &lat, &lng);
    return [[CLLocation alloc] initWithCoordinate:CLLocationCoordinate2DMake(lat, lng)
                                         altitude:bdLocation.altitude
                               horizontalAccuracy:bdLocation.horizontalAccuracy
                                 verticalAccuracy:bdLocation.verticalAccuracy
                                           course:bdLocation.course
                                            speed:bdLocation.speed
                                        timestamp:bdLocation.timestamp];
}

+ (CLLocationCoordinate2D)marsCoordFromBaiduCoord:(CLLocationCoordinate2D)coord
{
    
    CLLocation *bdLocation = [[CLLocation alloc]initWithLatitude:coord.latitude longitude:coord.longitude];
    CLLocation *marsLocation = [self marslocationFromBaiduLocation:bdLocation];
    return marsLocation.coordinate;
}


#pragma mark - 火星转百度

void transform_mars_to_baidu(double gg_lat, double gg_lon, double *bd_lat, double *bd_lon)
{
    double x = gg_lon, y = gg_lat;
    double z = sqrt(x * x + y * y) + 0.00002 * sin(y * x_pi);
    double theta = atan2(y, x) + 0.000003 * cos(x * x_pi);
    *bd_lon = z * cos(theta) + 0.0065;
    *bd_lat = z * sin(theta) + 0.006;
}


//火星转百度
+ (CLLocation*)baidulocationFromMarsLocation:(CLLocation *)marsLocation
{
    double lat = 0.0;
    double lng = 0.0;
    transform_mars_to_baidu(marsLocation.coordinate.latitude, marsLocation.coordinate.longitude, &lat, &lng);
    
    return [[CLLocation alloc] initWithCoordinate:CLLocationCoordinate2DMake(lat, lng)
                                         altitude:marsLocation.altitude
                               horizontalAccuracy:marsLocation.horizontalAccuracy
                                 verticalAccuracy:marsLocation.verticalAccuracy
                                           course:marsLocation.course
                                            speed:marsLocation.speed
                                        timestamp:marsLocation.timestamp];
}


//火星转百度-经纬度
+ (CLLocationCoordinate2D)baiduCoordFromMarsCoord:(CLLocationCoordinate2D)coord
{
    CLLocation *marsLocation = [[CLLocation alloc]initWithLatitude:coord.latitude longitude:coord.longitude];
    CLLocation *bdLocation = [self baidulocationFromMarsLocation:marsLocation];
    return bdLocation.coordinate;
}



@end
