//
//  NSNumber+CompareHelper.h
//  ClassExtensions
//
//  Created by zhigang on 17/4/18.
//  Copyright © 2017年 King. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSNumber (CompareHelper)

/**
 * Return the minimum / maximum between the receiver and another number (if equal, return the receiver)
 */
- (NSNumber *)minimumNumber:(NSNumber *)anotherNumber;
- (NSNumber *)maximumNumber:(NSNumber *)anotherNumber;

/**
 * Convenience methods for number comparisons. Easier to read than -[NSNumber compare:]
 */
- (BOOL)isLessThanNumber:(NSNumber *)number;
- (BOOL)isLessThanOrEqualToNumber:(NSNumber *)number;
- (BOOL)isGreaterThanNumber:(NSNumber *)number;
- (BOOL)isGreaterThanOrEqualToNumber:(NSNumber *)number;


@end
