//
//  NSNumber+CompareHelper.m
//  ClassExtensions
//
//  Created by zhigang on 17/4/18.
//  Copyright © 2017年 King. All rights reserved.
//

#import "NSNumber+CompareHelper.h"

@implementation NSNumber (CompareHelper)

#pragma mark Convenience methods

- (NSNumber *)minimumNumber:(NSNumber *)anotherNumber
{
    return [self isLessThanOrEqualToNumber:anotherNumber] ? self : anotherNumber;
}

- (NSNumber *)maximumNumber:(NSNumber *)anotherNumber
{
    return [self isGreaterThanOrEqualToNumber:anotherNumber] ? self : anotherNumber;
}

- (BOOL)isLessThanNumber:(NSNumber *)number
{
    NSParameterAssert(number);
    return [self compare:number] == NSOrderedAscending;
}

- (BOOL)isLessThanOrEqualToNumber:(NSNumber *)number
{
    NSParameterAssert(number);
    return [self compare:number] != NSOrderedDescending;
}

- (BOOL)isGreaterThanNumber:(NSNumber *)number
{
    NSParameterAssert(number);
    return [self compare:number] == NSOrderedDescending;
}

- (BOOL)isGreaterThanOrEqualToNumber:(NSNumber *)number
{
    NSParameterAssert(number);
    return [self compare:number] != NSOrderedAscending;
}


@end
